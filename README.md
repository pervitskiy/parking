# CSParking
### Мобильное приложение для работы с парковочным пространством города
#### Над проектом работают
- Кравченко Евгений(PM, front-end)
- Первцикий Дмитрий(back-end)
- Кондратков Артем(QA, front-end)
## Сервисы для работы
[Ссылка на Trello](https://trello.com/b/4TXTaXVE/parking)

[Ссылка на Miro](https://miro.com/app/board/o9J_lTNfROc=/)

## Документация

[Техническое задание](https://gitlab.com/pervitskiy/parking/-/blob/master/Documents/Tekhnicheskoe_zadanie.pdf)

[Курсовая работа](https://gitlab.com/pervitskiy/parking/-/blob/master/Documents/kursovaya.pdf)

[Отчет по ролям](https://gitlab.com/pervitskiy/parking/-/blob/master/Documents/Otchet_po_rolyam.pdf)

[Презентация](https://gitlab.com/pervitskiy/parking/-/blob/master/Documents/Presentation.pdf)

[Метрики](https://gitlab.com/pervitskiy/parking/-/blob/master/Documents/metrics.pdf)

[Swagger](https://parkingcs.xyz./api/swagger-ui/#/)

## Видео
[Презентация(видео, 7 минут)](https://cloud.mail.ru/public/6eBN/mZYDTAHJj)

[Презентация(видео) //видео, записанное до получения информации о том, что выступление ровно 7 минут](https://cloud.mail.ru/public/tvCC/5SWFSUeBj)

[Демовидео пользовательского интерфейса](https://disk.yandex.ru/i/Irse6gf8xU7z_Q)

[Демовидео работы приложения](https://cloud.mail.ru/public/sRt8/T3zN9xJwh)




