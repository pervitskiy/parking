package com.parking.backend.data.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "user_id", unique = true, nullable = false)
    private UUID userId;

    @Column(name = "telephone", nullable = false)
    private String telephone;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", nullable = false)
    private Date createDate;

    @Column(name = "password")
    private String password;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<Vehicle> vehicles = new HashSet<>();

    public void setVehicles(Set<Vehicle> vehicles) {
        if (vehicles!=null){
            if (!this.vehicles.isEmpty())
                this.vehicles.clear();
            this.vehicles.addAll(vehicles);
            vehicles.forEach(vehicle -> vehicle.setUser(this));
        }
    }
}
