package com.parking.backend.data.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "type_of_vehicle")
public class TypeVehicle implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_of_vehicle_id", unique = true, nullable = false)
    private Integer typeVehicleId;

    @Column(name = "type_name", nullable = false)
    private String typeName;

    @Column(name = "price", nullable = false)
    private Integer price;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "typeVehicle", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<Vehicle> vehicles = new HashSet<>();

    public void setVehicles(Set<Vehicle> vehicles) {
        if (vehicles!=null){
            if (!this.vehicles.isEmpty())
                this.vehicles.clear();
            this.vehicles.addAll(vehicles);
            vehicles.forEach(vehicle -> vehicle.setTypeVehicle(this));
        }
    }
}
