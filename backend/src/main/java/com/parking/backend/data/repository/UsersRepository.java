package com.parking.backend.data.repository;

import com.parking.backend.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UsersRepository extends JpaRepository<User, UUID> {
    @Query("select u from User u where u.telephone = ?1")
    User findByTelephone(String telephone);
}
