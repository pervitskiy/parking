package com.parking.backend.data.mapper;

import com.parking.backend.data.dto.UserDTO;
import com.parking.backend.data.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel="spring")
public interface UserMapper {

    public UserDTO toUserDTO(User user);

    @Mapping(target = "userId", source="userId", defaultExpression = "java(java.util.UUID.randomUUID())")
    public User toUser(UserDTO userDTO);
}
