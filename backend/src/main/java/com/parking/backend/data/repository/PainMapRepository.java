package com.parking.backend.data.repository;

import com.parking.backend.data.model.PainMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PainMapRepository extends JpaRepository<PainMap, Long> {
}
