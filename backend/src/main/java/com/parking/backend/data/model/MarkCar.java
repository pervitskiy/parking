package com.parking.backend.data.model;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "mark_car")
public class MarkCar implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mark_car_id", unique = true, nullable = false)
    private Integer markCarId;

    @Column(name = "mark_car_name", nullable = false)
    private String markCarName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "markCar", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<ModelCar> modelCars = new HashSet<>();

    public void setModelCars(Set<ModelCar> modelCars) {
        if (modelCars!=null){
            if (!this.modelCars.isEmpty())
                this.modelCars.clear();
            this.modelCars.addAll(modelCars);
            modelCars.forEach(modelCar -> modelCar.setMarkCar(this));
        }
    }
}
