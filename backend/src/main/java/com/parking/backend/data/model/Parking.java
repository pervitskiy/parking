package com.parking.backend.data.model;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "parking")
public class Parking implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "parking_id", unique = true, nullable = false)
    private Integer parkingId;

    @Column(name = "number_parking_space", nullable = false)
    private Integer numberParkingSpace;

    @Column(name = "number_disabled_parking_space")
    private Integer numberDisabledSpace;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "paid_parking_time_start", nullable = false)
    private Date startParkingTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "paid_parking_time_finish", nullable = false)
    private Date endParkingTime;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parking", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<PainMap> painsMap = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parking", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<ParkingSession> parkingSessions = new HashSet<>();

    public void setPainsMap(Set<PainMap> painsMap) {
        if (painsMap!=null){
            if (!this.painsMap.isEmpty())
                this.painsMap.clear();
            this.painsMap.addAll(painsMap);
            painsMap.forEach(painMap -> painMap.setParking(this));
        }
    }

    public void setParkingSessions(Set<ParkingSession> parkingSessions) {
        if (parkingSessions!=null){
            if (!this.parkingSessions.isEmpty())
                this.parkingSessions.clear();
            this.parkingSessions.addAll(parkingSessions);
            parkingSessions.forEach(parkingSession -> parkingSession.setParking(this));
        }
    }
}
