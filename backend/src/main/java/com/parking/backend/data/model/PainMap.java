package com.parking.backend.data.model;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "pain_map")
public class PainMap implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pain_map_id", unique = true, nullable = false)
    private Long painId;

    @Column(name = "coordinate_x", nullable = false)
    private String coordinateX;

    @Column(name = "coordinate_y", nullable = false)
    private String coordinateY;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "parking_id", nullable = false)
    private Parking parking;
}
