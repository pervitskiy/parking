package com.parking.backend.data.dto;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
public class UserDTO {
    private UUID userId;
    private String telephone;
    private String password;
}
