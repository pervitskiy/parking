package com.parking.backend.data.repository;

import com.parking.backend.data.model.TypeVehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeVehicleRepository extends JpaRepository<TypeVehicle, Integer> {
}
