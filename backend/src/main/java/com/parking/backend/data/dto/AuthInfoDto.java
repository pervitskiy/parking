package com.parking.backend.data.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Objects;
import java.util.UUID;

@Data
public class AuthInfoDto {
    private String token;
    private UUID id;

    public AuthInfoDto(String token, UUID id) {
        this.token = token;
        this.id = id;
    }
}
