package com.parking.backend.data.repository;

import com.parking.backend.data.model.ModelCar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModelCarRepository extends JpaRepository<ModelCar, Integer> {
}
