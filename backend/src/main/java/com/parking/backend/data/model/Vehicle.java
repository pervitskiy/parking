package com.parking.backend.data.model;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "vehicle")
public class Vehicle implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vehicle_id", unique = true, nullable = false)
    private Integer vehicleId;

    @Column(name = "vehicle_name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY,  cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "type_of_vehicle_id")
    private TypeVehicle typeVehicle;

    @ManyToOne(fetch = FetchType.LAZY,  cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "model_car_id")
    private ModelCar modelCar;

    @ManyToOne(fetch = FetchType.LAZY,  cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "vehicle", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<ParkingSession> parkingSessions = new HashSet<>();

    public void setParkingSessions(Set<ParkingSession> parkingSessions) {
        if (parkingSessions!=null){
            if (!this.parkingSessions.isEmpty())
                this.parkingSessions.clear();
            this.parkingSessions.addAll(parkingSessions);
            parkingSessions.forEach(parkingSession -> parkingSession.setVehicle(this));
        }
    }
}
