package com.parking.backend.data.repository;

import com.parking.backend.data.model.MarkCar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarkCarRepository extends JpaRepository<MarkCar, Integer> {
}
