package com.parking.backend.data.model;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "model_car")
public class ModelCar implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "model_car_id", unique = true, nullable = false)
    private Integer modelCarId;

    @Column(name = "model_car_name", nullable = false)
    private String modelCarName;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "mark_car_id", nullable = false)
    private MarkCar markCar;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "modelCar", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<Vehicle> vehicles = new HashSet<>();

    public void setVehicles(Set<Vehicle> vehicles) {
        if (vehicles!=null){
            if (!this.vehicles.isEmpty())
                this.vehicles.clear();
            this.vehicles.addAll(vehicles);
            vehicles.forEach(vehicle -> vehicle.setModelCar(this));
        }
    }
}
