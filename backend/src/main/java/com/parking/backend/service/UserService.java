package com.parking.backend.service;

import com.parking.backend.data.dto.AuthInfoDto;
import com.parking.backend.data.dto.UserDTO;
import com.parking.backend.data.mapper.UserMapper;
import com.parking.backend.data.model.User;
import com.parking.backend.data.repository.UsersRepository;
import com.parking.backend.security.JwtSupplier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserService {
    private UsersRepository userRepository;
    private UserMapper userMapper;
    private PasswordEncoder passwordEncoder;


    public UserService(UsersRepository userRepository, UserMapper userMapper, PasswordEncoder passwordEncoder, JwtSupplier jwtSupplier) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
        this.jwtSupplier = jwtSupplier;
    }

    private JwtSupplier jwtSupplier;

    //обработать ошибку, если не нашли
    public User getById(UUID id){
        return userRepository.findById(id).get();
    }

    public UserDTO toUserDTO(User user) {
        return userMapper.toUserDTO(user);
    }

    public AuthInfoDto loginUser(UserDTO userDTO) {
        var user = userRepository.findByTelephone(userDTO.getTelephone());
        if (passwordEncoder.matches(userDTO.getPassword(), user.getPassword())) {
            return generateTokenFromUser(user);
        }
        return null;
    }
    public UserDTO save(UserDTO userDTO)  {
        var password = passwordEncoder.encode(userDTO.getPassword());
        userDTO.setPassword(password);
        User user = userMapper.toUser(userDTO);
        return userMapper.toUserDTO(userRepository.save(user));
    }

    public AuthInfoDto generateTokenFromUser(User user) {
        return new AuthInfoDto(jwtSupplier.createTokenForUser(user.getTelephone()), user.getUserId());
    }
}
