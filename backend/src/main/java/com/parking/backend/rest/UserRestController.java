package com.parking.backend.rest;

import com.parking.backend.data.dto.UserDTO;
import com.parking.backend.data.mapper.UserMapper;
import com.parking.backend.data.model.User;
import com.parking.backend.data.repository.UsersRepository;
import com.parking.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Access;
import java.util.UUID;

@RestController
@RequestMapping("/user/")
public class UserRestController {
    @Autowired
    private UserService userService;

    @GetMapping(value = "{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> getUser(@PathVariable("userId") UUID userId){
        if (userId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User user = this.userService.getById(userId);
        return new ResponseEntity<>(this.userService.toUserDTO(user), HttpStatus.OK);
    }

    @PostMapping(value = "", consumes = "application/json", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> saveUser(@RequestBody UserDTO userDTO) throws DataIntegrityViolationException {
        if (userDTO == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        UserDTO user = this.userService.save(userDTO);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }
}
