package com.parking.backend.configs;

import com.parking.backend.security.FIlterForJwt;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletResponse;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final FIlterForJwt fIlterForJwt;

    public SecurityConfig(FIlterForJwt fIlterForJwt) {
        this.fIlterForJwt = fIlterForJwt;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception{
        httpSecurity
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(GET, "/user/*").authenticated()
                .antMatchers(GET, "/user/").authenticated()
                .antMatchers(POST, "/user/").anonymous()
                .antMatchers(PATCH, "/user/*").authenticated()
                .antMatchers(DELETE, "/user/").authenticated()
                .antMatchers(PUT, "/user/").authenticated()
                // Добавить новые методы
                .and()
                .addFilterBefore(fIlterForJwt, UsernamePasswordAuthenticationFilter.class)
                .logout()
                .logoutUrl("/security/logout")
                .permitAll()
                .logoutSuccessHandler((httpServletRequest, httpServletResponse, authentication) -> httpServletResponse.setStatus(HttpServletResponse.SC_OK));
    }
}
