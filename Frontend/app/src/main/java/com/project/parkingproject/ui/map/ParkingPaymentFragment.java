package com.project.parkingproject.ui.map;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.ParkingSession;
import com.project.parkingproject.data.dto.Transaction;
import com.project.parkingproject.data.requests.ParkingRequests;
import com.project.parkingproject.data.requests.UserRequests;
import com.yandex.metrica.YandexMetrica;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Callable;

public class ParkingPaymentFragment extends Fragment {

    private TextView parkingCostText;
    private TextView balanceText;
    private TextView paymentCostText;
    private TextView parkingTime;
    private Button payBtn;
    private SeekBar seekBar;
    private int time = 1;
    private int totalCost;
    private final ParkingPaymentFragment itSelf = this;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_parking_payment, container, false);
        YandexMetrica.reportEvent("У пользователя открылся фргамент оплаты парковки");
        parkingCostText = root.findViewById(R.id.parking_payment_parking_cost);
        balanceText = root.findViewById(R.id.parking_payment_balance);
        paymentCostText = root.findViewById(R.id.parking_payment_pay_cost);
        payBtn = root.findViewById(R.id.parking_payment_pay_btn);
        seekBar = root.findViewById(R.id.parking_payment_seek_bar);
        parkingTime = root.findViewById(R.id.parking_payment_time);

        seekBar.setMax(getArguments().getInt(MapFragment.ARGUMENT_TIME));

        final int costPerHour = getArguments().getInt(MapFragment.ARGUMENT_COST);
        final double balance = getArguments().getDouble(MapFragment.ARGUMENT_BALANCE);
        totalCost = costPerHour;

        parkingCostText.setText("Стоимость часа парковки: " + costPerHour + "р/час.");
        balanceText.setText("Текущий баланс: " + balance + "р.");
        parkingTime.setText("Длительность стоянки: 1ч.");
        paymentCostText.setText("Итоговая стоимость: " + totalCost + "р.");
        payBtn.setEnabled(true);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                parkingTime.setText("Длительность стоянки: " + progress + "ч.");
                time = progress;
                totalCost = costPerHour * progress;
                paymentCostText.setText("Итоговая стоимость: " + totalCost + "р.");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YandexMetrica.reportEvent("Пользователь нажал кнопку \"Оплатить\" во фрагменте оплаты парковки");
                if (totalCost > balance) {
                    createAlert();
                    return;
                }

                ParkingSession parkingSession = UserData.getInstance().getCurrentParkingSession();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                if (parkingSession != null) {
                    DateFormat jsonFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+'00:00");
                    try {
                        Date finish = jsonFormat.parse(parkingSession.getTimeFinish());
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(finish);
                        calendar.add(Calendar.HOUR_OF_DAY, time);
                        Date newFinish = calendar.getTime();
                        UserRequests.makeMoneyOperation(UserConfig.getToken(getContext()), UserConfig.getUserId(getContext()), new Transaction(totalCost, false), new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                parkingSession.setTimeFinish(dateFormat.format(newFinish));
                                ParkingRequests.continueParking(UserConfig.getToken(getContext()), parkingSession.getParkingSessionId(),
                                        parkingSession, new Callable<Void>() {
                                            @Override
                                            public Void call() throws Exception {
                                                Toast.makeText(getContext(), "Парковка продлена.", Toast.LENGTH_SHORT).show();
                                                getActivity().getSupportFragmentManager().beginTransaction().remove(itSelf).commit();
                                                //getActivity().getSupportFragmentManager().popBackStack();
                                                Navigation.findNavController(root).navigate(R.id.nav_map);
                                                return null;
                                            }
                                        }, new Callable<Void>() {
                                            @Override
                                            public Void call() throws Exception {
                                                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getContext());
                                                dlgAlert.setMessage("Проверьте соединение и попробуйте снова");
                                                dlgAlert.setTitle("Не удалось выполнить операцию");
                                                dlgAlert.setPositiveButton("OK", null);
                                                dlgAlert.create().show();
                                                return null;
                                            }
                                        });
                                return null;
                            }
                        }, new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getContext());
                                dlgAlert.setMessage("Проверьте соединение и попробуйте снова");
                                dlgAlert.setTitle("Не удалось выполнить операцию");
                                dlgAlert.setPositiveButton("OK", null);
                                dlgAlert.create().show();
                                return null;
                            }
                        });
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {

                    Date d = new Date();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(d);
                    calendar.add(Calendar.HOUR_OF_DAY, -3);
                    Date start = calendar.getTime();
                    calendar.setTime(start);
                    calendar.add(Calendar.HOUR_OF_DAY, time);
                    Date finish = calendar.getTime();
                    UserRequests.makeMoneyOperation(UserConfig.getToken(getContext()), UserConfig.getUserId(getContext()), new Transaction(totalCost, false), new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            YandexMetrica.reportEvent("С кошелька пользователя списались денежные средства");
                            ParkingRequests.subscribeOnParking(UserConfig.getToken(getContext()), getArguments().getInt(MapFragment.ARGUMENT_PARKING_ID), UserData.getInstance().getCar().getVehicleId(),
                                    new ParkingSession(dateFormat.format(finish), dateFormat.format(start)), new Callable<Void>() {
                                        @Override
                                        public Void call() throws Exception {
                                            Toast.makeText(getContext(), "Парковка оплачена.", Toast.LENGTH_LONG).show();
                                            YandexMetrica.reportEvent("Пользователь подписался на парковку");
                                            getActivity().getSupportFragmentManager().beginTransaction().remove(itSelf).commit();
                                            //getActivity().getSupportFragmentManager().popBackStack();
                                            Navigation.findNavController(root).navigate(R.id.nav_map);
                                            return null;
                                        }
                                    }, new Callable<Void>() {
                                        @Override
                                        public Void call() throws Exception {
                                            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getContext());
                                            dlgAlert.setMessage("Проверьте соединение и попробуйте снова");
                                            dlgAlert.setTitle("Не удалось выполнить операцию");
                                            dlgAlert.setPositiveButton("OK", null);
                                            dlgAlert.create().show();
                                            return null;
                                        }
                                    });
                            return null;
                        }
                    }, new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getContext());
                            dlgAlert.setMessage("Проверьте соединение и попробуйте снова");
                            dlgAlert.setTitle("Не удалось выполнить операцию");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.create().show();
                            return null;
                        }
                    });
                }
            }
        });

        return root;
    }

    private void createAlert(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
        dlgAlert.setMessage("Вы можете пополнить баланс во вкладке \"кошелёк\"." );
        dlgAlert.setTitle("Недостаточно средств");
        dlgAlert.setPositiveButton("Ок", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }
}
