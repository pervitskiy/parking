package com.project.parkingproject.ui.login;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.project.parkingproject.MainActivity;
import com.project.parkingproject.R;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.requests.UserRequests;
import com.project.parkingproject.data.services.Utils;
import com.project.parkingproject.ui.registration.RegistrationActivity;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

public class LoginActivity extends AppCompatActivity {

    private final int DELAY = 3000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText usernameEditText = findViewById(R.id.userphone);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final Button registerLinkButton = findViewById(R.id.toRegActivity);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        final boolean[] isCorrectPhone = {false};

        usernameEditText.setText("+");

        registerLinkButton.setEnabled(true);


        TextWatcher afterPhoneChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginButton.setEnabled(false);

                if (s.toString().equals("")) {
                    usernameEditText.setText("+");
                    usernameEditText.setSelection(usernameEditText.getSelectionEnd() + 1);
                } else {
                    isCorrectPhone[0] = Utils.validatePhone((s.toString()));
                    if (isCorrectPhone[0]) loginButton.setEnabled(true);
                }
            }
        };
        usernameEditText.addTextChangedListener(afterPhoneChangedListener);

        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                UserConfig.clearUserInfo(getApplicationContext());
                UserRequests.login(usernameEditText.getText().toString(), passwordEditText.getText().toString(), getApplicationContext(), new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        new Timer().schedule(new TimerTask() {
                            @Override
                            public void run() {
                                loadingProgressBar.setVisibility(View.INVISIBLE);
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                            }
                        }, DELAY);
                        UserData.getInstance().requestUserData(UserConfig.getToken(getApplicationContext()), UserConfig.getUserId(getApplicationContext()));
                        return null;
                    }
                },
                new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                loadingProgressBar.setVisibility(View.INVISIBLE);
                                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getApplicationContext());
                                dlgAlert.setMessage("Проверьте имя пользователя и пароль");
                                dlgAlert.setTitle("Не удалось залогинться");
                                dlgAlert.setPositiveButton("OK", null);
                                dlgAlert.create().show();
                                Log.e("LOGIN FAILED", "check password");
                                return null;
                            }
                        });


                return;
            }
        });

        registerLinkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
                return;
            }
        });
    }


    private void login() {

    }


    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}