package com.project.parkingproject.ui.wallet;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.Transaction;
import com.project.parkingproject.data.requests.UserRequests;
import com.yandex.metrica.YandexMetrica;

import java.util.concurrent.Callable;

public class WalletFragment extends Fragment {

    private static final String MONEY_LABEL = "Текущий баланс: ";
    private WalletViewModel walletViewModel;

    private EditText moneyCount;
    private TextView moneyInfo;
    private Button payButton;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        walletViewModel =
                new ViewModelProvider(this).get(WalletViewModel.class);
        View root = inflater.inflate(R.layout.fragment_wallet, container, false);

        moneyCount = root.findViewById(R.id.wallet_amount);
        moneyInfo = root.findViewById(R.id.wallet_current_balance);
        payButton = root.findViewById(R.id.wallet_topUpBtn);

        moneyInfo.setText(new StringBuilder().append(MONEY_LABEL).append(UserData.getInstance().getUser().getMoney()).toString());
        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YandexMetrica.reportEvent("Клик по кнопке пополнения кошелька");
                addMoney();
            }
        });

        Spinner paymentMethod = (Spinner) root.findViewById(R.id.wallet_paymentMethod);
        String[] methods = {"Банковская карта", "PayPal", "QIWI"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, methods);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paymentMethod.setAdapter(adapter);

        YandexMetrica.reportEvent("Переход в окно пополнения кошелька");
        return root;
    }

    private void addMoney() {
        try {
            double money = Double.parseDouble(moneyCount.getText().toString());
            if (money > 0) {
                UserRequests.makeMoneyOperation(UserConfig.getToken(getContext()), UserConfig.getUserId(getContext()), new Transaction(money, true), new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        YandexMetrica.reportEvent("Баланс успешно пополнен");
                        Toast.makeText(getContext(), "Баланс пополнен!", Toast.LENGTH_SHORT).show();
                        moneyInfo.setText(new StringBuilder().append(MONEY_LABEL).append(UserData.getInstance().getUser().getMoney()).toString());
                        return null;
                    }
                }, new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getContext());
                        dlgAlert.setMessage("Проверьте соединение и попробуйте снова");
                        dlgAlert.setTitle("Не удалось пополнить баланс");
                        dlgAlert.setPositiveButton("OK", null);
                        dlgAlert.create().show();
                        return null;
                    }
                });
            }
            else {
                Toast.makeText(getContext(), "Неправильно введена сумма.", Toast.LENGTH_SHORT).show();
            }
        } catch (NumberFormatException e) {
            Toast.makeText(getContext(), "Произошла ошибка.", Toast.LENGTH_SHORT).show();
            return;
        }
    }
}