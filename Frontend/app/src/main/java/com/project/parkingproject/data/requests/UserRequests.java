package com.project.parkingproject.data.requests;

import android.content.Context;
import android.util.Log;

import com.project.parkingproject.data.RetrofitSingleton;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.LoginPostResult;
import com.project.parkingproject.data.dto.Transaction;
import com.project.parkingproject.data.dto.User;
import com.project.parkingproject.data.services.UserService;

import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRequests {

    public static void createUser(String telephone, String password, String firstName, String lastName, Callable<Void> onSuccess, Callable<Void> onFailure) {
        UserService retrofit = RetrofitSingleton.getInstance().create(UserService.class);
        Call<Void> userCall = retrofit.saveUser(new User(password, telephone, firstName, lastName));
        userCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Create user request was succesfull");
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "Create user request wasn't succesfull");
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("REQUEST", "Create user request wasn't succesfull");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void login(String telephone, String password, Context context,  Callable<Void> onSucces, Callable<Void> onFailure) {
        UserService retrofit = RetrofitSingleton.getInstance().create(UserService.class);
        Call<LoginPostResult> loginCall = retrofit.loginUser(new User(password, telephone));
        final LoginPostResult[] result = new LoginPostResult[1];
        loginCall.enqueue(new Callback<LoginPostResult>() {
            @Override
            public void onResponse(Call<LoginPostResult> call, Response<LoginPostResult> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "LOGIN REQUEST SUCCESFULL\nToken: " + response.body().getToken()
                            + "\nId: " + response.body().getId());
                    UserConfig.saveUserInfo(response.body().getId(), response.body().getToken(), response.body().isAdmin(), context);

                    try {
                        onSucces.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return;
                }
                Log.e("REQUEST", "Login request wasn't succesfull");
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LoginPostResult> call, Throwable t) {
                Log.e("REQUEST", "Login request wasn't succesfull");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void getUser (String token, String id) {
        UserService retrofit = RetrofitSingleton.getInstance().create(UserService.class);
        Call<User> loginCall = retrofit.getUser("Bearer " + token, id);
        loginCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "GET USER REQUEST SUCCESFULL");
                    UserData.getInstance().initUser(response.body());
                    return;
                }
                Log.e("REQUEST", "GET USER request wasn't succesfull" + response.code());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("REQUEST", "GET USER request wasn't succesfull");
                t.printStackTrace();
            }
        });
    }

    public static void updateUser(String token, String id, User user, Callable<Void> onSucess, Callable<Void> onFailure) {
        UserService retrofit = RetrofitSingleton.getInstance().create(UserService.class);
        Call<User> updateCall = retrofit.updateUser(token, id, user);
        final LoginPostResult[] result = new LoginPostResult[1];
        updateCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Update USER REQUEST SUCCESFULL");
                    UserData.getInstance().initUser(response.body());
                    try {
                        onSucess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "Update USER request wasn't succesfull" + response.code());
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("REQUEST", "Update USER request wasn't succesfull");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void deleteUser (String token, String adminId, String userId, Callable<Void> onSuccess) {
        UserService retrofit = RetrofitSingleton.getInstance().create(UserService.class);
        Call<Void> deleteCall = retrofit.deleteUser("Bearer " + token, adminId, userId);
        deleteCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "delete USER REQUEST SUCCESFULL");
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "delete USER request wasn't succesfull" + response.code());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("REQUEST", "delte USER request wasn't succesfull");
                t.printStackTrace();
            }
        });
    }

    public static void makeMoneyOperation(String token, String userId, Transaction transaction, Callable<Void> onSuccess, Callable<Void> onFailure) {
        UserService retrofit = RetrofitSingleton.getInstance().create(UserService.class);
        Call<User> transactionCall = retrofit.makeMoneyTransaction("Bearer " + token, userId, transaction);
        transactionCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Money operation REQUEST SUCCESFULL");
                    UserData.getInstance().getUser().setMoney(response.body().getMoney());
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "Money operation request wasn't succesfull" + response.code());
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("REQUEST", "Money operation request wasn't succesfull");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
