package com.project.parkingproject.data.dto;

public class User {

    private String password;

    private String telephone;

    private String userId;

    private String firstName;

    private String lastName;

    private double money;


    public User(String password, String telephone, String firstName, String lastName) {
        this.password = password;
        this.telephone = telephone;
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public User(String password, String telephone) {
        this.password = password;
        this.telephone = telephone;
    }

    public User( String userId, String password, String telephone, String firstName, String lastName) {
        this.password = password;
        this.telephone = telephone;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getPassword() {
        return password;
    }
}
