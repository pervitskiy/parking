package com.project.parkingproject.data.services;

import java.util.regex.Pattern;

public class Utils {

    public static boolean validatePhone(String phone) {
        return Pattern.compile("^((\\+7)([0-9]{10}))$").matcher(phone).matches();
    }

    public static boolean validatePassword(String password) {
        return Pattern.compile("\\w{6,}").matcher(password).matches();
    }

    public static boolean validateName(String name) {
        return Pattern.compile("^[a-zA-Z]+$").matcher(name).matches() ||
                Pattern.compile("^[а-яА-Я]+$").matcher(name).matches();
    }

    public static boolean validateCarNumber(String number) {
        return Pattern.compile("^(([а-яА-Я]{1})(\\d{3})([а-яА-Я]{2})(\\d{2,3}))$").matcher(number).matches();
    }

    public static String convertToSimpleTime(String time) {
        return time.charAt(11) == '0' ? time.substring(12, 16) : time.substring(11, 16);
    }

    public static String convertToDateTime(String time) {
        return time.length() == 4 ? "2021-05-01T0" + time + ":30.853+00:00" :
                "2021-05-01T" + time + ":30.853+00:00";
    }

    public static String convertToDateTimeFromDay(String day) {
        String res = day.substring(6, 10) + "-" + day.substring(3, 5) + "-" + day.substring(0, 2) +
                "T00:00:30.853+00:00";
        //Log.d("Try parse   " + day, "result is   " + res);
        return res;
    }

}
