package com.project.parkingproject.data.requests;

import android.util.Log;

import com.project.parkingproject.data.RetrofitSingleton;
import com.project.parkingproject.data.dto.Parking;
import com.project.parkingproject.data.services.ParkingService;

import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FullParkingRequest {

    private Parking parking;

    public void getParkingRequest(String token, int parkingId, int typeVehicleId, Callable<Void> onSuccess, Callable<Void> onFailure) {
        ParkingService retrofit = RetrofitSingleton.getInstance().create(ParkingService.class);
        Call<Parking> parkingsCall = retrofit.getFullParkinInfo("Bearer "  + token, parkingId, typeVehicleId);
        parkingsCall.enqueue(new Callback<Parking>() {
            @Override
            public void onResponse(Call<Parking> call, Response<Parking> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Get full parking info request was succesfull");
                    parking = response.body();

                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "Get full parking info request wasn't succesfull" + response.code());
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Parking> call, Throwable t) {
                Log.e("REQUEST", "Getting full parking request failed");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Parking getParking() {
        return parking;
    }
}
