package com.project.parkingproject.data.requests;

import android.util.Log;

import com.project.parkingproject.data.RetrofitSingleton;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.Parking;
import com.project.parkingproject.data.dto.ParkingSession;
import com.project.parkingproject.data.dto.ShorParkingInfo;
import com.project.parkingproject.data.services.ParkingService;

import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParkingRequests {

    public static void createParking(Parking parking, String token) {
        ParkingService retrofit = RetrofitSingleton.getInstance().create(ParkingService.class);
        Call<Void> parkingCall = retrofit.saveParking("Bearer "  + token, new Parking[]{parking});
        parkingCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Create parking request was succesfull");
                    return;
                }
                Log.e("REQUEST", "Create parking request wasn't succesfull" + response.code());

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("REQUEST", "Create parking request failed");
                t.printStackTrace();
            }
        });
    }

    public static void getAllParkings(String token, Callable<Void> onSuccess) {
        ParkingService retrofit = RetrofitSingleton.getInstance().create(ParkingService.class);
        Call<ShorParkingInfo[]> parkingsCall = retrofit.getAllParkings("Bearer "  + token);
        parkingsCall.enqueue(new Callback<ShorParkingInfo[]>() {
            @Override
            public void onResponse(Call<ShorParkingInfo[]> call, Response<ShorParkingInfo[]> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Get all parkings request was succesfull");
                    UserData.getInstance().initParkings(response.body());

                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }

                Log.e("REQUEST", "Get parkings request wasn't succesfull" + response.code());
            }

            @Override
            public void onFailure(Call<ShorParkingInfo[]> call, Throwable t) {
                Log.e("REQUEST", "Getting parkings request failed");
                t.printStackTrace();
            }
        });
    }

    public static void subscribeOnParking(String token, int parkingId, int vehicleId, ParkingSession body, Callable<Void> onSuccess, Callable<Void> onFailure) {
        ParkingService retrofit = RetrofitSingleton.getInstance().create(ParkingService.class);
        Call<ParkingSession> parkingsCall = retrofit.subscribeOnParking("Bearer "  + token, parkingId, vehicleId, body);
        parkingsCall.enqueue(new Callback<ParkingSession>() {
            @Override
            public void onResponse(Call<ParkingSession> call, Response<ParkingSession> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Subsribe on parking  request was succesfull");
                    ParkingSession session = response.body();
                    if(session != null) {
                        UserData.getInstance().setCurrentParkingSession(session);
                        UserData.getInstance().getParkingSessionsList().add(session);
                    }
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }

                Log.e("REQUEST", "Subscribe on parking request wasn't succesfull" + response.code());
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ParkingSession> call, Throwable t) {
                Log.e("REQUEST", "Subscribing on parking request failed");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void continueParking(String token, long parkingSessionId, ParkingSession body, Callable<Void> onSuccess, Callable<Void> onFailure) {
        ParkingService retrofit = RetrofitSingleton.getInstance().create(ParkingService.class);
        Call<ParkingSession> parkingsCall = retrofit.continueParkingSession("Bearer "  + token, parkingSessionId, body);
        parkingsCall.enqueue(new Callback<ParkingSession>() {
            @Override
            public void onResponse(Call<ParkingSession> call, Response<ParkingSession> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Continue parking request was succesfull");
                    ParkingSession session = response.body();
                    if(session != null) {
                        UserData.getInstance().setCurrentParkingSession(session);
                    }
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }

                Log.e("REQUEST", "Continue parking request wasn't succesfull" + response.code());
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ParkingSession> call, Throwable t) {
                Log.e("REQUEST", "Continue parking request failed");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void updateParking(String token, int parkingId, Parking parking, Callable<Void> onSuccess) {
        ParkingService retrofit = RetrofitSingleton.getInstance().create(ParkingService.class);
        Call<Parking> parkingCall = retrofit.updateParking("Bearer "  + token, parkingId, parking);
        parkingCall.enqueue(new Callback<Parking>() {
            @Override
            public void onResponse(Call<Parking> call, Response<Parking> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Update parking request was succesfull");

                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "Update parking request wasn't succesfull" + response.code());

            }

            @Override
            public void onFailure(Call<Parking> call, Throwable t) {
                Log.e("REQUEST", "Update parking request failed");
                t.printStackTrace();
            }
        });
    }

    public static void deleteParking(String token, String adminId, int parkingId, Callable<Void> onSuccess) {
        ParkingService retrofit = RetrofitSingleton.getInstance().create(ParkingService.class);
        Call<Void> parkingsCall = retrofit.deleteParking("Bearer "  + token, adminId, parkingId);
        parkingsCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Delete parking  request was succesfull");

                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }

                Log.e("REQUEST", "Delete parking request wasn't succesfull" + response.code());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("REQUEST", "Delete parking request failed");
                t.printStackTrace();
            }
        });
    }
}
