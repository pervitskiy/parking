package com.project.parkingproject.data.dto;

import com.google.gson.annotations.SerializedName;

public class Mark {

    private int markCarId;

    private String markCarName;

    @SerializedName("modelsCarDTO")
    private Model[] models;

    public Mark(int markCarId, String markCarName, Model[] models) {
        this.markCarId = markCarId;
        this.markCarName = markCarName;
        this.models = models;
    }

    public int getMarkCarId() {
        return markCarId;
    }

    public void setMarkCarId(int markCarId) {
        this.markCarId = markCarId;
    }

    public String getMarkCarName() {
        return markCarName;
    }

    public void setMarkCarName(String markCarName) {
        this.markCarName = markCarName;
    }

    public Model[] getModels() {
        return models;
    }

    public void setModels(Model[] models) {
        this.models = models;
    }
}
