package com.project.parkingproject.ui.map;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.project.parkingproject.R;
import com.project.parkingproject.TransitionActivity;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.Car;
import com.project.parkingproject.data.dto.MapPoint;
import com.project.parkingproject.data.dto.Parking;
import com.project.parkingproject.data.dto.ParkingSession;
import com.project.parkingproject.data.dto.ShorParkingInfo;
import com.project.parkingproject.data.dto.User;
import com.project.parkingproject.data.requests.FullParkingRequest;
import com.project.parkingproject.data.requests.ParkingRequests;
import com.project.parkingproject.data.services.Utils;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKit;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.geometry.Polyline;
import com.yandex.mapkit.location.Location;
import com.yandex.mapkit.location.LocationListener;
import com.yandex.mapkit.location.LocationStatus;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.InputListener;
import com.yandex.mapkit.map.Map;
import com.yandex.mapkit.map.MapObject;
import com.yandex.mapkit.map.MapObjectTapListener;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.map.PolylineMapObject;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.metrica.YandexMetrica;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

public class MapFragment extends Fragment {

    private MapKit mapKit;
    private MapViewModel mapViewModel;
    private MapView mapview;
    private ConstraintLayout createPanel;
    private Button addParkingsBtn;
    private Button updateParkingBtn;
    private Button startAddParkingsBtn;
    private Button stopAddParkingsBtn;
    private ImageButton geoBtn;
    private List<Point> parkingPoints = new ArrayList<>();
    private List<PlacemarkMapObject> placemarksToDelete = new ArrayList<>();
    private HashMap<MapObject, ShorParkingInfo> parkingMap;

    //admin create panel;
    private EditText parkingNameText;
    private EditText priceText;
    private EditText startTimeText;
    private EditText endTimeText;
    private EditText totalPlaceText;
    private EditText truckPlaceText;
    private EditText invalidPlaceText;

    //parking popup;
    private View popupView;
    private PopupWindow popupWindow;
    private TextView popupNameText;
    private TextView popupCostText;
    private TextView popupWorkTimeText;
    private TextView popupTotalPlacesText;
    private TextView popupTruckPlacesText;
    private TextView popupInvalidPlacesText;

    private View root;

    private ParkingPaymentFragment fragment = new ParkingPaymentFragment();
    private MapFragment itself = this;

    private Button popupPayBtn;
    private Button popupUpdateBtn;
    private Button popupDeleteBtn;
    private Button popupStatisticBtn;

    public static final String ARGUMENT_TIME = "time";
    public static final String ARGUMENT_COST = "cost";
    public static final String ARGUMENT_BALANCE = "balance";
    public static final String ARGUMENT_PARKING_ID = "ParkingID";


    private final InputListener mapInputListener = new InputListener() {
        @Override
        public void onMapTap(@NonNull Map map, @NonNull Point point) {
            parkingPoints.add(point);
            placemarksToDelete.add(mapview.getMap().getMapObjects().addPlacemark(point));
        }

        @Override
        public void onMapLongTap(@NonNull Map map, @NonNull Point point) {

        }
    };

    private final MapObjectTapListener tapListener = new MapObjectTapListener() {
        @Override
        public boolean onMapObjectTap(@NonNull MapObject mapObject, @NonNull Point point) {
            Log.d("MAP CLICK", "Polyline clicked");
            FullParkingRequest req = new FullParkingRequest();
            YandexMetrica.reportEvent("Пользователь запросил информацию о парковке");
            int typeVehicle = UserData.getInstance().getCar() != null ? UserData.getInstance().getCar().getTypeVehicleId() : 2;
            req.getParkingRequest(UserConfig.getToken(getContext()), parkingMap.get(mapObject).getParkingId(), typeVehicle, new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    createPopup(req.getParking());
                    YandexMetrica.reportEvent("Пользователь получил и посмотрел  информацию о парковке");
                    return null;
                }
            }, new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
                    dlgAlert.setMessage("Не удалось загрузить информацию о парковке. Проверьте соединение и попробуйте снова");
                    dlgAlert.setTitle("Проблема с соединением");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.create().show();
                    return null;
                }
            });

            return true;
        }
    };


    public View onCreateView(@NonNull LayoutInflater popupInflater,
                             ViewGroup container, Bundle savedInstanceState) {
        User u = UserData.getInstance().getUser();
        List<Car> c = UserData.getInstance().getCarList();
        List<ShorParkingInfo> p = UserData.getInstance().getParkingList();
        List<ParkingSession> s = UserData.getInstance().getParkingSessionsList();
        if(UserData.getInstance().getUser() == null ||
        UserData.getInstance().getCarList() == null || UserData.getInstance().getParkingList() == null || UserData.getInstance().getParkingSessionsList() == null) {
            createAlert();
        }
        if(UserData.getInstance().getCar() == null) {
            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
            dlgAlert.setMessage("Добавьте транспортное средство");
            dlgAlert.setTitle("У вас нет добавленных ТС, добавьте их в разделе \"Автомобили\"");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.create().show();
        }

        mapViewModel =
                new ViewModelProvider(this).get(MapViewModel.class);
        root = popupInflater.inflate(R.layout.fragment_map, container, false);

        mapview = root.findViewById(R.id.mapview);
        mapKit = MapKitFactory.getInstance();
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 255);
        }
        mapview.getMap().move(
                new CameraPosition(new Point(51.65658328735572, 39.206018602273524), 14.0f, 0.0f, 0.0f),
                new Animation(Animation.Type.SMOOTH, 0),
                null);

        YandexMetrica.reportEvent("У пользователя открылась карта");
        //moveCameraToUserPosition();

        parkingMap = new HashMap<>();
        setParkingOnMap(UserData.getInstance().getParkingList());

        popupInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popupView = popupInflater.inflate(R.layout.parking_popup, null);

        int width = ConstraintLayout.LayoutParams.MATCH_PARENT;
        int height = ConstraintLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;

        popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupNameText = popupView.findViewById(R.id.parkingPopup_name);
        popupCostText = popupView.findViewById(R.id.parkingPopup_cost);
        popupWorkTimeText = popupView.findViewById(R.id.parkingPopup_workTime);
        popupTotalPlacesText = popupView.findViewById(R.id.parkingPopup_totalPlaces);
        popupTruckPlacesText = popupView.findViewById(R.id.parkingPopup_truckPlaces);
        popupInvalidPlacesText = popupView.findViewById(R.id.parkingPopup_invalidPlaces);

        popupPayBtn = popupView.findViewById(R.id.parkingPopupPayBtn);
        popupStatisticBtn = popupView.findViewById(R.id.parkingPopupStatisticBtn);
        popupPayBtn.setEnabled(UserData.getInstance().getCar() != null);

        if (UserConfig.isAdmin(getContext())) {
            popupUpdateBtn = popupView.findViewById(R.id.parkingPopupUpdateBtn);
            popupDeleteBtn = popupView.findViewById(R.id.parkingPopupDeleteBtn);
            popupStatisticBtn = popupView.findViewById(R.id.parkingPopupStatisticBtn);
            popupUpdateBtn.setEnabled(true);
            popupUpdateBtn.setVisibility(View.VISIBLE);
            popupDeleteBtn.setEnabled(true);
            popupDeleteBtn.setVisibility(View.VISIBLE);
            popupPayBtn.setEnabled(false);
            popupPayBtn.setVisibility(View.GONE);

            startAddParkingsBtn = root.findViewById(R.id.startAddParkings);
            startAddParkingsBtn.setVisibility(View.VISIBLE);
            startAddParkingsBtn.setEnabled(true);
            createPanel = root.findViewById(R.id.adminpanel);
            parkingNameText = root.findViewById(R.id.admin_parking_name);
            priceText = root.findViewById(R.id.admin_parking_price);
            startTimeText = root.findViewById(R.id.admin_parking_time_start);
            endTimeText = root.findViewById(R.id.admin_parking_time_finish);
            totalPlaceText = root.findViewById(R.id.admin_parking_total_places);
            truckPlaceText = root.findViewById(R.id.admin_parking_trucks_places);
            invalidPlaceText = root.findViewById(R.id.admin_parking_invalid_places);

            addParkingsBtn = root.findViewById(R.id.addParkingsBtn);
            addParkingsBtn.setVisibility(View.VISIBLE);
            addParkingsBtn.setEnabled(true);

            updateParkingBtn = root.findViewById(R.id.updateParkingBtn);

            stopAddParkingsBtn = root.findViewById(R.id.stopAddParkings);


            startAddParkingsBtn.setOnClickListener(new View.OnClickListener() { // включаешь режим добавления
                @Override
                public void onClick(View v) {
                    stopAddParkingsBtn.setVisibility(View.VISIBLE);
                    stopAddParkingsBtn.setEnabled(true);

                    startAddParkingsBtn.setVisibility(View.INVISIBLE);
                    startAddParkingsBtn.setEnabled(false);

                    mapview.getMap().addInputListener(mapInputListener);
                }
            });

            stopAddParkingsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stopAddParkingsBtn.setEnabled(false);
                    stopAddParkingsBtn.setVisibility(View.INVISIBLE);
                    createPanel.setVisibility(View.VISIBLE);
                    mapview.getMap().removeInputListener(mapInputListener);
                }
            });


            addParkingsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createPanel.setVisibility(View.INVISIBLE);
                    startAddParkingsBtn.setVisibility(View.VISIBLE);
                    startAddParkingsBtn.setEnabled(true);

                    MapPoint[] resPoints = new MapPoint[parkingPoints.size()];
                    for (int i = 0; i < resPoints.length; i++) {
                        resPoints[i] = new MapPoint(parkingPoints.get(i));
                    }
                    Parking parking = new Parking(parkingNameText.getText().toString(),
                            Integer.parseInt(totalPlaceText.getText().toString()),
                            Integer.parseInt(invalidPlaceText.getText().toString()),
                            Integer.parseInt(truckPlaceText.getText().toString()),
                            Integer.parseInt(priceText.getText().toString()),
                            Utils.convertToDateTime(startTimeText.getText().toString()), Utils.convertToDateTime(endTimeText.getText().toString()), resPoints);

                    ParkingRequests.createParking(parking, UserConfig.getToken(getContext()));
                    PolylineMapObject polyline = mapview.getMap().getMapObjects().addPolyline(new Polyline(parkingPoints));
                    polyline.addTapListener(tapListener);

                    placemarksToDelete.clear();
                    parkingPoints.clear();
                    //TODO чистить весь текст
                }
            });
        }
        geoBtn = root.findViewById(R.id.geo);
        geoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveCameraToUserPosition();
            }
        });

        return root;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 255) {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
                dlgAlert.setMessage("Приложение не работает без разрешения использования геопозиции");
                dlgAlert.setTitle("Разрешите использовать геопозицию");
                dlgAlert.setPositiveButton("Перезайти", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getActivity(), TransitionActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
                dlgAlert.setCancelable(true);
                dlgAlert.setNegativeButton("Выход", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    }
                });
                dlgAlert.create().show();
            }
        }
    }

    private void setParkingOnMap(List<ShorParkingInfo> parkings) {

        if (parkings == null) {
            Log.e("PARKING ERROR", "Parking list is null");
            return;
        }

        for (ShorParkingInfo parking : parkings) {
            MapPoint[] points = parking.getPoints();
            List<Point> resPoints = new ArrayList<>(points.length);

            for (int i = 0; i < points.length; i++) {
                resPoints.add(new Point(Double.parseDouble(points[i].getX()), Double.parseDouble(points[i].getY())));
            }

            PolylineMapObject polyline = mapview.getMap().getMapObjects().addPolyline(new Polyline(resPoints));
            parkingMap.put(polyline, parking);
            polyline.addTapListener(tapListener);
        }
    }

    public void moveCameraToPosition(@NonNull Point target) {
        mapview.getMap().move(
                new CameraPosition(target, 15.0f, 0.0f, 0.0f),
                new Animation(Animation.Type.SMOOTH, 2), null);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapview.onStart();
        MapKitFactory.getInstance().onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapview.onStop();
        MapKitFactory.getInstance().onStop();
        try {
            getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        } catch (Exception e) {}
        try {
            getActivity().getSupportFragmentManager().beginTransaction().remove(itself).commit();
        } catch (Exception e) {}
//        getActivity().getSupportFragmentManager().getFragments().clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        } catch (Exception e) {}
        try {
            getActivity().getSupportFragmentManager().beginTransaction().remove(itself).commit();
        } catch (Exception e) {}
    }

    public void moveCameraToUserPosition() {
        mapKit.createLocationManager().requestSingleUpdate(new LocationListener() {
            @Override
            public void onLocationUpdated(@NonNull Location location) {
                Log.d("Check", location.getPosition().getLongitude() + " " + location.getPosition().getLatitude());
                mapview.getMap().move(
                        new CameraPosition(location.getPosition(), 14.0f, 0.0f, 0.0f),
                        new Animation(Animation.Type.SMOOTH, 0),
                        null);
            }

            @Override
            public void onLocationStatusUpdated(@NonNull LocationStatus locationStatus) {

            }
        });

    }

    private void createPopup(Parking parking) {
        fillPopup(parking);
        popupWindow.showAtLocation(getView(), Gravity.BOTTOM, 0, 0);

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    private void fillPopup(Parking parking) {
        popupCostText.setText("Стоимость часа стоянки: " + parking.getPrice() + "р/час.");
        popupWorkTimeText.setText("Режим работы: " + Utils.convertToSimpleTime(parking.getStartParkingTime()) + " - "
                + Utils.convertToSimpleTime(parking.getEndParkingTime()));
        popupNameText.setText("Название парковки: " + parking.getParkingName());
        popupTotalPlacesText.setText("Количество мест всего: " + parking.getNumberParkingSpace());
        popupTruckPlacesText.setText("Мест для грузовых авто: " + parking.getNumberTrackSpace());
        popupInvalidPlacesText.setText("Мест для инвалидов: " + parking.getNumberDisabledSpace());
        Car userActiveCar = UserData.getInstance().getCar();

        if(userActiveCar == null || userActiveCar.getTypeVehicleId() == 1 && parking.getNumberTrackSpace() <= 0 ||
                userActiveCar.getTypeVehicleId() == 2 && parking.getNumberParkingSpace() <= 0 ||
                userActiveCar.isInvalid() && parking.getNumberParkingSpace() <= 0 && parking.getNumberDisabledSpace() <=0 ||
                UserData.getInstance().getCurrentParkingSession() != null) {
            popupPayBtn.setEnabled(false);
        }
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+'hh:ss");
            Date end = dateFormat.parse(parking.getEndParkingTime());
            Date start = dateFormat.parse(parking.getStartParkingTime());
            Date now = new Date();
            if (start.getHours() == now.getHours() && start.getMinutes() > now.getMinutes() ||
                    end.getHours() == now.getHours() && end.getMinutes() < now.getMinutes() ||
                    start.getHours() > now.getHours() || end.getHours() < now.getHours()) {
                popupPayBtn.setEnabled(false);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (UserConfig.isAdmin(getContext())) {
            popupDeleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ParkingRequests.deleteParking(UserConfig.getToken(getContext()), UserConfig.getUserId(getContext()), parking.getParkingId(), new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            Toast.makeText(getContext(), "Парковка удалена", Toast.LENGTH_SHORT).show();
                            createPanel.setVisibility(View.GONE);
                            createPanel.setEnabled(false);
                            return null;
                        }
                    });
                }
            });
            popupUpdateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                    createPanel.setVisibility(View.VISIBLE);
                    updateParkingBtn.setVisibility(View.VISIBLE);
                    updateParkingBtn.setEnabled(true);
                    startAddParkingsBtn.setVisibility(View.GONE);
                    startAddParkingsBtn.setEnabled(false);

                    parkingNameText.setText(parking.getParkingName());
                    priceText.setText(parking.getPrice() + "");
                    startTimeText.setText(Utils.convertToSimpleTime(parking.getStartParkingTime()));
                    endTimeText.setText(Utils.convertToSimpleTime(parking.getEndParkingTime()));
                    totalPlaceText.setText(String.valueOf(parking.getNumberParkingSpace()));
                    truckPlaceText.setText(String.valueOf(parking.getNumberTrackSpace()));
                    invalidPlaceText.setText(String.valueOf(parking.getNumberDisabledSpace()));

                    updateParkingBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Parking newParking = new Parking(parking.getParkingId(), parkingNameText.getText().toString(), Integer.parseInt(totalPlaceText.getText().toString()),
                                    Integer.parseInt(invalidPlaceText.getText().toString()), Integer.parseInt(truckPlaceText.getText().toString()),
                                    Integer.parseInt(priceText.getText().toString()), Utils.convertToDateTime(startTimeText.getText().toString()), Utils.convertToDateTime(endTimeText.getText().toString()),
                                    parking.getPoints());
                            ParkingRequests.updateParking(UserConfig.getToken(getContext()), parking.getParkingId(), newParking, new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    Toast.makeText(getContext(), "Парковка обновлена", Toast.LENGTH_SHORT);
                                    createPanel.setVisibility(View.GONE);
                                    createPanel.setEnabled(false);
                                    startAddParkingsBtn.setVisibility(View.VISIBLE);
                                    startAddParkingsBtn.setEnabled(true);
                                    return null;
                                }
                            });

                        }
                    });
                }
            });
        } else {
            popupPayBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YandexMetrica.reportEvent("Пользователь нажал на кнопку оплатить парковку");
                    int time = 0;
                    try {
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+'hh:ss");
                        Date end = dateFormat.parse(parking.getEndParkingTime());
                        Date now = new Date();
                        time = end.getHours() - now.getHours() + 1;
                        if (end.getHours() == now.getHours() && end.getMinutes() < now.getMinutes() ||
                                end.getHours() < now.getHours()) {
                            popupWindow.dismiss();
                            return;
                        }
                    } catch (ParseException e) {
                        return;
                    }
                    popupWindow.dismiss();
                    Bundle bundle = new Bundle();
                    bundle.putInt(ARGUMENT_COST, parking.getPrice());
                    bundle.putDouble(ARGUMENT_BALANCE, UserData.getInstance().getUser().getMoney());
                    bundle.putInt(ARGUMENT_PARKING_ID, parking.getParkingId());
                    bundle.putInt(ARGUMENT_TIME, time);
                    fragment.setArguments(bundle);

                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, fragment).commit();
                    root.setVisibility(View.GONE);
                    root.setEnabled(false);
                    //                String start = Utils.convertToDateTime("8:30");
//                String finish = Utils.convertToDateTime("9:30");
//                ParkingRequests.subscribeOnParking(UserConfig.getToken(getContext()), parking.getParkingId(), UserData.getInstance().getCar().getVehicleId(),
//                        new ParkingSession(start, finish), new Callable<Void>() {
//                            @Override
//                            public Void call() throws Exception {
//                                return null;
//                            }
//                        });
                }

            });
        }
        popupStatisticBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createStatisticsAlert(parking);
            }
        });
    }

    private void createAlert(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
        dlgAlert.setCancelable(false);
        dlgAlert.setMessage("Произошла проблема с загрузкой данных");
        dlgAlert.setTitle("Данные не были загружены");
        dlgAlert.setPositiveButton("Перезайти", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getActivity(), TransitionActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        dlgAlert.setNegativeButton("Выход", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });
        dlgAlert.create().show();
    }

    private void createStatisticsAlert(Parking parking) {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
        dlgAlert.setCancelable(false);
        dlgAlert.setTitle("Статистика парковки " + parking.getParkingName());
        StringBuilder statString = new StringBuilder();
        int placeCount = parking.getNumberParkingSpace() +
                parking.getNumberDisabledSpace() + parking.getNumberTrackSpace();
        for (java.util.Map.Entry<Integer, Integer> pair: parking.getStatistics().entrySet()) {
            statString.append(pair.getKey() + ":00 - "  + pair.getValue() + "/" + placeCount + "\n");
        }
        dlgAlert.setMessage(statString.toString());
        dlgAlert.setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dlgAlert.create().show();
    }

}