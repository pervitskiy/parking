package com.project.parkingproject.data.dto;

public class CarPostData {

    private String name;

    private String numberVehicle;

    private int markId;

    private int modelId;

    private int typeVehicleId;

    private String userId;

    private boolean invalid;

    private boolean chosen;

    public CarPostData(String name, String numberVehicle, int markId, int modelId, int typeVehicleId, String userId, boolean invalid, boolean chosen) {
        this.name = name;
        this.numberVehicle = numberVehicle;
        this.markId = markId;
        this.modelId = modelId;
        this.typeVehicleId = typeVehicleId;
        this.userId = userId;
        this.invalid = invalid;
        this.chosen = chosen;
    }
}
