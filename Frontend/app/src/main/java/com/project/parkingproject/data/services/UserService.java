package com.project.parkingproject.data.services;

import com.project.parkingproject.data.dto.LoginPostResult;
import com.project.parkingproject.data.dto.ParkingSession;
import com.project.parkingproject.data.dto.Transaction;
import com.project.parkingproject.data.dto.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    @GET("user/{userId}")
    Call<User> getUser(@Header ("Authorization") String token, @Path("userId") String id);

    @POST("user/")
    Call<Void> saveUser(@Body User body);

    @POST("security/login")
    Call<LoginPostResult> loginUser(@Body User body);

    @PUT("user/{userId}")
    Call<User> updateUser(@Header ("Authorization") String token, @Path("userId") String id, @Body User body);

    @GET("user/{userId}/getAllSessionParking")
    Call<ParkingSession[]> getAllParkingSession(@Header ("Authorization") String token, @Path("userId") String id);

    @GET("user/getAll/{adminId}")
    Call<User[]> getAllUsers(@Header ("Authorization") String token, @Path("adminId") String adminId);

    @DELETE("user/{adminId}/deleteByAmdin/user/{userId}")
    Call<Void> deleteUser(@Header ("Authorization") String token, @Path("adminId") String adminId, @Path("userId") String userId);

    @POST("user/{userId}")
    Call<User> makeMoneyTransaction(@Header ("Authorization") String token, @Path("userId") String userId, @Body Transaction transaction);

}
