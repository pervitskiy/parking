package com.project.parkingproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.ui.login.LoginActivity;

import java.util.Timer;
import java.util.TimerTask;

public class TransitionActivity extends AppCompatActivity {

    private final int DELAY = 8000;

    private ImageView background;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transition);


        background = findViewById(R.id.transition_background);
        background.setImageResource(R.mipmap.transition_background);

        String token = UserConfig.getToken(getApplicationContext());

        if(token != null && !token.isEmpty()) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    goToNextActivity(true);
                }
            }, DELAY);

            UserData.getInstance().requestUserData(token, UserConfig.getUserId(getApplicationContext()));

        }else {
            goToNextActivity(false);
        }
    }



    private void goToNextActivity(boolean hasToken) {
        Intent intent = hasToken ? new Intent(TransitionActivity.this, MainActivity.class)
                : new Intent(TransitionActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

}