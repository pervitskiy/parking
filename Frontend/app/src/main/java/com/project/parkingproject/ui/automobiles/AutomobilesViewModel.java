package com.project.parkingproject.ui.automobiles;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AutomobilesViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public AutomobilesViewModel() {
        mText = new MutableLiveData<>();
        //mText.setValue("This is gallery fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}