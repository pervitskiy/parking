package com.project.parkingproject.ui.create_automobiles;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.CarPostData;
import com.project.parkingproject.data.dto.Mark;
import com.project.parkingproject.data.dto.Model;
import com.project.parkingproject.data.requests.CarRequests;
import com.project.parkingproject.data.requests.MarkRequests;
import com.project.parkingproject.data.services.Utils;
import com.yandex.metrica.YandexMetrica;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

public class CreateAutomobileFragment extends Fragment {

    private CreateAutomobileViewModel createAutomobileViewModel;

    private Button createBtn;
    private TextView modelText;
    private EditText numberText;
    private EditText nameText;
    private RadioButton typeCarRButton;
    private RadioButton typeTruckRButton;
    private CheckBox isInvalid;
    private ProgressBar progressBar;
    private Spinner markaSpinner;
    private Spinner modelSpinner;

    private List<Mark> marki;

    private boolean isCorrectCarNumber;
    private boolean isModelSelected;
    private boolean isCorrectName;

    private CreateAutomobileFragment itSelf = this;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        createAutomobileViewModel =
                new ViewModelProvider(this).get(CreateAutomobileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_create_automobile, container, false);

        progressBar = root.findViewById(R.id.progress);

        createBtn = root.findViewById(R.id.create_automobile_btn);
        modelText = root.findViewById(R.id.create_automobile_modelText);
        numberText = root.findViewById(R.id.create_automobile_number);
        nameText = root.findViewById(R.id.create_automobile_name);
        typeCarRButton = root.findViewById(R.id.create_automobile_type_car);
        typeTruckRButton = root.findViewById(R.id.create_automobile_type_truck);
        isInvalid = root.findViewById(R.id.create_automobile_isInvalid);
        markaSpinner = (Spinner) root.findViewById(R.id.create_automobile_marka);
        modelSpinner = (Spinner) root.findViewById(R.id.create_automobile_model);

        createBtn.setEnabled(false);

        progressBar.setVisibility(View.VISIBLE);
        lockAll();

        MarkRequests markRequest = new MarkRequests();
        markRequest.getAllMarks(UserConfig.getToken(getContext()), new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                progressBar.setVisibility(View.INVISIBLE);
                unlockAll();
                marki = Arrays.asList(markRequest.getResult()); // сюда все марки

                String[] markiStrArray = new String[marki.size()];
                for (int i = 0; i < marki.size(); i++) {
                    markiStrArray[i] = marki.get(i).getMarkCarName();
                }

                ArrayAdapter<String> markiAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, markiStrArray);
                markiAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                markaSpinner.setAdapter(markiAdapter);
                return null;
            }
        }, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
                dlgAlert.setMessage("Проверьте соединение и попробуйте снова");
                dlgAlert.setTitle("Не удалось выполнить операцию");
                dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().getSupportFragmentManager().beginTransaction().remove(itSelf).commit();
                        getActivity().getSupportFragmentManager().popBackStack();
                        Navigation.findNavController(root).navigate(R.id.nav_automobiles);
                    }
                });
                dlgAlert.create().show();
                return null;
            }
        });


        markaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Mark mark = findMarkaByName((String) markaSpinner.getSelectedItem());

                if (mark == null)
                    return;

                String[] models = new String[mark.getModels().length];
                for (int i = 0; i < models.length; i++) {
                    models[i] = mark.getModels()[i].getModelCarName();
                }
                ArrayAdapter<String> modelsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, models);
                modelsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                modelSpinner.setAdapter(modelsAdapter);
                modelSpinner.setVisibility(View.VISIBLE);
                modelText.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                modelSpinner.setVisibility(View.INVISIBLE);
                modelSpinner.setEnabled(false);
                modelText.setVisibility(View.INVISIBLE);
                modelText.setEnabled(false);
                createBtn.setEnabled(false);
            }
        });

        modelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                isModelSelected = true;
                //createBtn.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        TextWatcher afterCarNumberChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                createBtn.setEnabled(false);
                isCorrectCarNumber = Utils.validateCarNumber(s.toString());
                if (isCorrectCarNumber && isModelSelected && isCorrectName) createBtn.setEnabled(true);
            }
        };

        numberText.addTextChangedListener(afterCarNumberChangedListener);

        TextWatcher afterNameChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                createBtn.setEnabled(false);
                isCorrectName = Utils.validateName(s.toString());
                if (isCorrectCarNumber && isModelSelected && isCorrectName) createBtn.setEnabled(true);
            }
        };

        nameText.addTextChangedListener(afterNameChangedListener);

        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mark mark = findMarkaByName((String) markaSpinner.getSelectedItem());
                Model model = findModelByName(mark, (String) modelSpinner.getSelectedItem());
                String number = numberText.getText().toString();
                String name = nameText.getText().toString();
                int typeId = typeCarRButton.isChecked() ? 2 : 1;
                boolean chosen = UserData.getInstance().getCarList().isEmpty();
                if (number.isEmpty() || name.isEmpty()) {
                    //TODO просить пользователя заполрнить
                }
                progressBar.setVisibility(View.VISIBLE);
                lockAll();
                CarRequests.createCar(new CarPostData(name, number, mark.getMarkCarId(), model.getModelCarId(), typeId, UserConfig.getUserId(getContext()), isInvalid.isChecked(), chosen)
                        , UserConfig.getToken(getContext()), new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                progressBar.setVisibility(View.INVISIBLE);
                                unlockAll();
                                Toast.makeText(getContext(), "Автомобиль добавлен!", Toast.LENGTH_SHORT);
                                YandexMetrica.reportEvent("Добавлен новый автомобиль");
                                getActivity().getSupportFragmentManager().beginTransaction().remove(itSelf).commit();
                                getActivity().getSupportFragmentManager().popBackStack();
                                Navigation.findNavController(root).navigate(R.id.nav_automobiles);
                                return null;
                            }
                        }, new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
                                progressBar.setVisibility(View.INVISIBLE);
                                unlockAll();
                                dlgAlert.setMessage("Проверьте соединение с интернетом или уникальность номера ТС и попробуйте снова");
                                dlgAlert.setTitle("Опреация не удалась");
                                dlgAlert.setPositiveButton("OK", null);
                                dlgAlert.create().show();
                                return null;
                            }
                        });

            }
        });
        YandexMetrica.reportEvent("Открыто окно создания автомобиля");

        return root;
    }

    private Mark findMarkaByName(String name) {
        for (Mark marka : marki) {
            if (marka.getMarkCarName().equals(name))
                return marka;
        }
        return null;
    }

    private Model findModelByName(Mark marka, String name) {
        for (Model model : marka.getModels()) {
            if (model.getModelCarName().equals(name))
                return model;
        }
        return null;
    }

    private void lockAll() {
        createBtn.setEnabled(false);
        modelText.setEnabled(false);
        numberText.setEnabled(false);
        nameText.setEnabled(false);
        typeCarRButton.setEnabled(false);
        typeTruckRButton.setEnabled(false);
        markaSpinner.setEnabled(false);
        modelSpinner.setEnabled(false);
    }

    private void unlockAll() {
        createBtn.setEnabled(true);
        modelText.setEnabled(true);
        numberText.setEnabled(true);
        nameText.setEnabled(true);
        typeCarRButton.setEnabled(true);
        typeTruckRButton.setEnabled(true);
        markaSpinner.setEnabled(true);
        modelSpinner.setEnabled(true);
    }

}