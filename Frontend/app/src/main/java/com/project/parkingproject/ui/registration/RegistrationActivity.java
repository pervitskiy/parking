package com.project.parkingproject.ui.registration;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.project.parkingproject.R;
import com.project.parkingproject.data.requests.UserRequests;
import com.project.parkingproject.data.services.Utils;
import com.project.parkingproject.ui.login.LoginActivity;

import java.util.concurrent.Callable;

public class RegistrationActivity extends AppCompatActivity { // пока просто разметка, логики и моделей нет

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        final EditText userPhoneEditText = findViewById(R.id.userphone);
        final EditText userName = findViewById(R.id.username);
        final EditText userSurname = findViewById(R.id.userSurname);
        final EditText passwordEditText = findViewById(R.id.password);
        final EditText passwordAcceptText = findViewById(R.id.passwordAccept);
        final Button loginButton = findViewById(R.id.login);
        final Button registerLinkButton = findViewById(R.id.toLoginActivity);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        final boolean[] isCorrectPhone = {false};
        final boolean[] isCorrectPassword = {false};
        final boolean[] arePasswordsEquals = {false};

        registerLinkButton.setEnabled(true);

        userPhoneEditText.setText("+");


        TextWatcher afterPhoneChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginButton.setEnabled(false);

                if (s.toString().equals("")) {
                    userPhoneEditText.setText("+");
                    userPhoneEditText.setSelection(userPhoneEditText.getSelectionEnd() + 1);
                } else {
                    isCorrectPhone[0] = Utils.validatePhone((s.toString()));
                    if (isCorrectPassword[0] && isCorrectPhone[0] && arePasswordsEquals[0]) loginButton.setEnabled(true);
                }
            }
        };
        userPhoneEditText.addTextChangedListener(afterPhoneChangedListener);


        TextWatcher afterPasswordChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginButton.setEnabled(false);
                isCorrectPassword[0] = Utils.validatePassword((s.toString()));
                if (isCorrectPassword[0] && isCorrectPhone[0] && arePasswordsEquals[0]) loginButton.setEnabled(true);
            }
        };

        passwordEditText.addTextChangedListener(afterPasswordChangedListener);


        TextWatcher afterPasswordAcceptChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginButton.setEnabled(false);
                arePasswordsEquals[0] = passwordEditText.getText().toString().equals(passwordAcceptText.getText().toString());
                if (isCorrectPassword[0] && isCorrectPhone[0] && arePasswordsEquals[0]) loginButton.setEnabled(true);
            }
        };

        passwordAcceptText.addTextChangedListener(afterPasswordAcceptChangedListener);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = userName.getText().toString().trim();
                String surname = userSurname.getText().toString().trim();
                UserRequests.createUser(userPhoneEditText.getText().toString(), passwordEditText.getText().toString(),
                        name, surname, new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                Toast.makeText(getApplicationContext(), "Регистрация прошла успешно!", Toast.LENGTH_SHORT);
                                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                                startActivity(intent);
                                return null;
                            }
                        }, new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(RegistrationActivity.this);
                                dlgAlert.setMessage("Не удалось зарегистрироваться. Проверьте соединение  и попробуйте снова." +
                                        "Возможно, существует пользователь с данным номером телефона");
                                dlgAlert.setTitle("Проблема с соединением или данный номер телефона занят");
                                dlgAlert.setPositiveButton("OK", null);
                                dlgAlert.create().show();
                                return null;
                            }
                        });

                return;
            }
        });

        registerLinkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(intent);
                return;
            }
        });

    }
}