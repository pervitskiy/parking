package com.project.parkingproject.ui.users;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.dto.User;
import com.project.parkingproject.data.requests.UserRequests;

import java.util.concurrent.Callable;

public class ShowUserFragment extends Fragment {

    private EditText userFirstName;
    private EditText userLastName;
    private TextView userID;
    private EditText userPhone;

    private String cachedFirstName;
    private String cachedLastName;
    private String cachedPhone;
    private String password;
    private String id;

    private Button acceptBtn;
    private Button deleteBtn;
    private Button resetPasswordBtn;

    public static final String ARGUMENT_USER_FIRSTNAME = "firstName";
    public static final String ARGUMENT_USER_LASTNAME = "lastName";
    public static final String ARGUMENT_USER_PHONE = "phone";
    public static final String ARGUMENT_USER_ID = "id";
    public static final String ARGUMENT_USER_PASSWORD = "password";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_show_user, container, false);

        userFirstName = root.findViewById(R.id.show_user_first_name);
        userLastName = root.findViewById(R.id.show_user_last_name);
        userID = root.findViewById(R.id.show_user_id);
        userPhone = root.findViewById(R.id.show_user_phone);

        acceptBtn = root.findViewById(R.id.show_user_accept_btn);
        deleteBtn = root.findViewById(R.id.show_user_delete_btn);
        resetPasswordBtn = root.findViewById(R.id.show_user_reset_password_btn);


        cachedFirstName = getArguments().get(ARGUMENT_USER_FIRSTNAME).toString();
        cachedLastName = getArguments().get(ARGUMENT_USER_LASTNAME).toString();
        cachedPhone = getArguments().get(ARGUMENT_USER_PHONE).toString();
        id = getArguments().get(ARGUMENT_USER_ID).toString();
        password = getArguments().getString(ARGUMENT_USER_PASSWORD);

        userFirstName.setText(cachedFirstName);
        userLastName.setText(cachedLastName);
        userID.setText(id);
        userPhone.setText(cachedPhone);

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser();
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserRequests.deleteUser(UserConfig.getToken(getContext()), UserConfig.getUserId(getContext()), id, new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        Toast.makeText(getContext(), "Пользователь удалён", Toast.LENGTH_SHORT).show();
                        return null;
                    }
                });
            }
        });

        return root;
    }


    private void updateUser() {
        String firstName = userFirstName.getText().toString();
        String lastName = userLastName.getText().toString();
        String phone = userPhone.getText().toString();
        if(!phone.equals(cachedPhone) || !firstName.equals(cachedFirstName) || !lastName.equals(cachedLastName)) {
            UserRequests.updateUser(UserConfig.getToken(getContext()), id, new User(id, password, phone, firstName, lastName), new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    Toast.makeText(getContext(), "Пользователь обновлен", Toast.LENGTH_SHORT).show();
                    return null;
                }
            }, new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    return null;
                }
            });
        }
    }

}
