package com.project.parkingproject.ui.users;

import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.dto.User;
import com.project.parkingproject.data.requests.AllUsersRequest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;

public class UsersFragment extends Fragment {

    private ScrollView scrollView;
    private Button findBtn;
    private EditText numberEditText;
    private boolean isCorrectNumber;
    private ProgressBar progressBar;
    private List<User> allUsers;
    private ShowUserFragment showUserFragment;

    private ShowUserFragment fragment = new ShowUserFragment();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_users, container, false);

        scrollView = root.findViewById(R.id.users_scrollView);
        findBtn = root.findViewById(R.id.users_find_btn);
        numberEditText = root.findViewById(R.id.users_number);
        progressBar = root.findViewById(R.id.users_progress_bar);

        progressBar.setVisibility(View.VISIBLE);
        AllUsersRequest request = new AllUsersRequest();
        request.getAllUsers(UserConfig.getToken(getContext()), UserConfig.getUserId(getContext()), new Callable<Void>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public Void call() throws Exception {
                allUsers = request.getData();
                progressBar.setVisibility(View.GONE);
                showUsers(allUsers, root);
                return null;
            }
        });

        findBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (numberEditText.getText().toString().isEmpty()) {
                    showUsers(allUsers, root);
                    return;
                }
                String number = numberEditText.getText().toString();
                List<User> targetUser = new ArrayList<>();
                allUsers.forEach(user -> {
                    if (user.getTelephone().equals(number)) {
                        targetUser.add(user);
                    }
                });
                showUsers(targetUser, root);
            }
        });


        return root;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showUsers(List<User> users, View root) {
        scrollView.removeAllViews();
        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setGravity(Gravity.LEFT);
        scrollView.addView(layout);
        users.sort(new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getLastName().toLowerCase().compareTo(o2.getLastName().toLowerCase());
            }
        });
        users.forEach(user -> {

            TextView name = new TextView(getContext());
            name.setText(user.getLastName() + "\t" + user.getFirstName() + "\t" + user.getTelephone());
            name.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    100));
            name.setGravity(Gravity.CENTER);
            name.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.text_view_border));
            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ShowUserFragment.ARGUMENT_USER_FIRSTNAME, user.getFirstName() != null ? user.getFirstName() : "");
                    bundle.putString(ShowUserFragment.ARGUMENT_USER_LASTNAME, user.getLastName() != null ? user.getLastName(): "");
                    bundle.putString(ShowUserFragment.ARGUMENT_USER_ID, user.getUserId());
                    bundle.putString(ShowUserFragment.ARGUMENT_USER_PHONE, user.getTelephone());
                    bundle.putString(ShowUserFragment.ARGUMENT_USER_PASSWORD, user.getPassword());

                    fragment.setArguments(bundle);
                    root.setEnabled(false);
                    root.setVisibility(View.INVISIBLE);
                    getActivity().getSupportFragmentManager().beginTransaction().add(R.id.nav_host_fragment, fragment).commit();
                }
            });

            layout.addView(name);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //todo show new fragment
                }
            });
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        } catch (Exception e) {}
    }
}
