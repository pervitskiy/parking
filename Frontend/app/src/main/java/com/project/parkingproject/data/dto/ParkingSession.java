package com.project.parkingproject.data.dto;

import com.project.parkingproject.data.services.Utils;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ParkingSession {

    private Long parkingSessionId;

    @SerializedName("parkingDetailDto")
    private Parking parking;

    private String timeFinish;

    private String timeStart;

    @SerializedName("vehicleDto")
    private Car car;

    public ParkingSession(String timeFinish, String timeStart) {
        this.timeFinish = timeFinish;
        this.timeStart = timeStart;
    }

    public ParkingSession(Parking parking, String timeFinish, String timeStart, Car car) {
        this.parking = parking;
        this.timeFinish = timeFinish;
        this.timeStart = timeStart;
        this.car = car;
    }

    public Parking getParking() {
        return parking;
    }

    public String getTimeFinish() {
        return timeFinish;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public Car getCar() {
        return car;
    }

    public int getPrice() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+'hh:ss");
        try {
            Date end = dateFormat.parse(timeFinish);
            Date start = dateFormat.parse(timeStart);
            double pricePetHour = parking.getPrice() + (car.getTypeVehicleId() == 2 ? 5 : 10);

            if (start.getHours() > end.getHours())
                return (int) ((24 - start.getHours() + end.getHours()) * pricePetHour);
            else if (start.getHours() == end.getHours())
                return (int) pricePetHour;
            else if (start.getMinutes() < end.getMinutes())
                return (int) ((end.getHours() - start.getHours() + 1) * pricePetHour);
            else
                return (int) ((end.getHours() - start.getHours()) * pricePetHour);

        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }

    }

    public Long getParkingSessionId() {
        return parkingSessionId;
    }

    public String getEndTimeInSimpleFormat() {
        return getTimeInSimpleFormat(timeFinish);
    }

    public String getStartTimeInSimpleFormat() {
        return getTimeInSimpleFormat(timeStart);
    }

    private String getTimeInSimpleFormat(String time) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+'hh:ss");
        try {
            Date res = dateFormat.parse(time);
            Calendar c = Calendar.getInstance();
            c.setTime(res);
            c.add(Calendar.HOUR_OF_DAY, 3);
            return Utils.convertToSimpleTime(dateFormat.format(c.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void setTimeFinish(String timeFinish) {
        this.timeFinish = timeFinish;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }
}
