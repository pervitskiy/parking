package com.project.parkingproject.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitSingleton {

    private static volatile Retrofit retrofit;
    //private static final String BASE_URL = "https://parkingcs.xyz/api/";
    private static final String BASE_URL = "http://35.246.56.98:9000/api/";


    public static Retrofit getInstance() {
        if(retrofit == null)
            synchronized (RetrofitSingleton.class) {
                if(retrofit == null) {
                    Gson gson = new GsonBuilder().serializeNulls().create();
                    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                    logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                    OkHttpClient client = new OkHttpClient.Builder()
                            .addInterceptor(logging)
                            .build();
                    retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create(gson))
                            .client(client)
                            .build();
                }
            }
        return retrofit;
    }
}
