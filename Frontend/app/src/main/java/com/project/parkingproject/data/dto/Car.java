package com.project.parkingproject.data.dto;

public class Car {

    private Integer vehicleId;

    private String car;

    private int markaId;

    private int modelId;

    private String name;

    private String numberVehicle;

    private String typeVehicle;

    private int typeVehicleId;

    private String userId;

    private boolean invalid;

    private boolean chosen;


    public Car(String car, int markaId, int modelId, String name, String numberVehicle, String typeVehicle, int typeVehicleId, String userId, boolean invalid, boolean chosen) {
        this.car = car;
        this.markaId = markaId;
        this.modelId = modelId;
        this.name = name;
        this.numberVehicle = numberVehicle;
        this.typeVehicle = typeVehicle;
        this.typeVehicleId = typeVehicleId;
        this.userId = userId;
        this.invalid = invalid;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public int getMarkaId() {
        return markaId;
    }

    public void setMarkaId(int markaId) {
        this.markaId = markaId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumberVehicle() {
        return numberVehicle;
    }

    public void setNumberVehicle(String numberVehicle) {
        this.numberVehicle = numberVehicle;
    }

    public String getTypeVehicle() {
        return typeVehicle;
    }

    public void setTypeVehicle(String typeVehicle) {
        this.typeVehicle = typeVehicle;
    }

    public int getTypeVehicleId() {
        return typeVehicleId;
    }

    public void setTypeVehicleId(int typeVehicleId) {
        this.typeVehicleId = typeVehicleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public boolean isChosen() {
        return chosen;
    }
}
