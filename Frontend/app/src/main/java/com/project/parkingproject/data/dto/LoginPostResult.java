package com.project.parkingproject.data.dto;

public class LoginPostResult {

    private String id;

    private String token;

    private boolean admin;

    public LoginPostResult(String id, String token, boolean admin) {
        this.id = id;
        this.token = token;
        this.admin = admin;
    }

    public String getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public boolean isAdmin() {
        return admin;
    }
}
