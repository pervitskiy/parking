package com.project.parkingproject.data.dto;

public class ShorParkingInfo {

    private int parkingId;

    private MapPoint[] points;

    public ShorParkingInfo(int parkingId, MapPoint[] points) {
        this.parkingId = parkingId;
        this.points = points;
    }

    public int getParkingId() {
        return parkingId;
    }

    public MapPoint[] getPoints() {
        return points;
    }
}
