package com.project.parkingproject.ui.automobiles;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.Car;
import com.project.parkingproject.ui.create_automobiles.CreateAutomobileFragment;
import com.yandex.metrica.YandexMetrica;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class AutomobilesFragment extends Fragment {

    private AutomobilesViewModel automobilesViewModel;
    private Fragment addCarFragment = new CreateAutomobileFragment();
    private ShowCarFragment showCarFragment;
    private List<Button> automobiles = new ArrayList();
    private TextView hasNoAutoText;
    private Car currentCar;
    private ScrollView scroll;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        automobilesViewModel =
                new ViewModelProvider(this).get(AutomobilesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_automobiles, container, false);
        final Button addAutoBtn = root.findViewById(R.id.to_newAuto);

        hasNoAutoText = root.findViewById(R.id.automobiles_has_no_auto_text);
        scroll = root.findViewById(R.id.automobiles_scroll);

        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setBackgroundColor(Color.parseColor("#959190"));
        layout.setGravity(Gravity.LEFT);
//        if (savedInstanceState.containsKey("carAdded") && savedInstanceState.getBoolean("carAdded")) {
//            Toast.makeText(getContext(), "Автомобиль добавлен!", Toast.LENGTH_SHORT).show();
//        }


        if (UserData.getInstance().getCarList() != null && !UserData.getInstance().getCarList().isEmpty()) {
            scroll.addView(layout);
            Car activeCar = UserData.getInstance().getCar();
            UserData.getInstance().getCarList().forEach(car -> {

                TextView name = new TextView(getContext());
                if (car != null && activeCar != null && car.getVehicleId() == activeCar.getVehicleId())
                    name.setText("Активное авто:\t\t" + car.getName() + "\t\t" + car.getNumberVehicle());
                else name.setText(car.getName() + "\t\t" + car.getNumberVehicle());
                name.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        100));
                name.setGravity(Gravity.CENTER);
                name.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.text_view_border));
                layout.addView(name);

                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showCarFragment = new ShowCarFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(ShowCarFragment.ARGUMENT_CAR_NAME, car.getName());
                        bundle.putString(ShowCarFragment.ARGUMENT_CAR_NUMBER, car.getNumberVehicle());
                        bundle.putString(ShowCarFragment.ARGUMENT_CAR_TYPE, car.getTypeVehicle());
                        bundle.putString(ShowCarFragment.ARGUMENT_CAR_MARK_MODEL, car.getCar());
                        bundle.putInt(ShowCarFragment.ARGUMENT_CAR_ID, car.getVehicleId());
                        if (car.isInvalid())
                            bundle.putString(ShowCarFragment.ARGUMENT_CAR_INVALID, "Для людей с ограниченными возможностями");
                        else
                            bundle.putString(ShowCarFragment.ARGUMENT_CAR_INVALID, "");
                        showCarFragment.setArguments(bundle);
                        root.setEnabled(false);
                        root.setVisibility(View.INVISIBLE);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, showCarFragment).commit();
                    }
                });

            });
        } else {
            hasNoAutoText.setVisibility(View.VISIBLE);
        }


        addAutoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, addCarFragment).commit();
            }
        });
        YandexMetrica.reportEvent("Открыто окно просмотра автомобилей");

        return root;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (showCarFragment != null)
            try {
                getActivity().getSupportFragmentManager().beginTransaction().remove(showCarFragment).commit();
            } catch (Exception e) {

            }
        try {
            getActivity().getSupportFragmentManager().beginTransaction().remove(addCarFragment).commit();
        } catch (Exception e) {
        }
    }
}