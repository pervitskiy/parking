package com.project.parkingproject.ui.automobiles;

import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.Car;
import com.project.parkingproject.data.requests.CarRequests;

import java.util.List;
import java.util.concurrent.Callable;

public class ShowCarFragment extends Fragment {

    public static final String ARGUMENT_CAR_NAME = "carName";
    public static final String ARGUMENT_CAR_NUMBER = "carNumber";
    public static final String ARGUMENT_CAR_TYPE = "carType";
    public static final String ARGUMENT_CAR_INVALID = "isInvalid";
    public static final String ARGUMENT_CAR_ID = "carId";
    public static final String ARGUMENT_CAR_MARK_MODEL = "carMarkModel";

    private EditText carNameEdit;
    private TextView carNumber;
    private TextView carType;
    private TextView isInvalid;
    private TextView markModelText;
    private CheckBox isActiveCB;
    private Button acceptBtn;
    private Button backBtn;

    private ShowCarFragment itSelf = this;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_show_car, container, false);

        carNameEdit = root.findViewById(R.id.automobile_edit_name);
        carNumber = root.findViewById(R.id.automobile_number);
        carType = root.findViewById(R.id.automobile_type);
        isInvalid = root.findViewById(R.id.automobile_is_invalid);
        isActiveCB = root.findViewById(R.id.automobile_is_active);
        acceptBtn = root.findViewById(R.id.automobile_acceptBtn);
        backBtn = root.findViewById(R.id.automobile_backBtn);
        markModelText = root.findViewById(R.id.automobile_mark_model_text);

        carNameEdit.setText(getArguments().get(ARGUMENT_CAR_NAME).toString());
        carNumber.setText(getArguments().get(ARGUMENT_CAR_NUMBER).toString());
        carType.setText(getArguments().get(ARGUMENT_CAR_TYPE).toString());
        markModelText.setText(getArguments().get(ARGUMENT_CAR_MARK_MODEL).toString());
        isInvalid.setText(getArguments().get(ARGUMENT_CAR_INVALID).toString());



        boolean isNotActive = getArguments().getInt(ARGUMENT_CAR_ID) != UserData.getInstance().getCar().getVehicleId();
        isActiveCB.setChecked(!isNotActive);

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                if (isNotActive && isActiveCB.isChecked()) {
                    setCarActive(getArguments().getInt(ARGUMENT_CAR_ID));
                }
                String carName = carNameEdit.getText().toString();
                if(!carName.isEmpty() && !carName.equals(getArguments().get(ARGUMENT_CAR_NAME).toString())) {
                    Car car = UserData.getInstance().getCarList().stream().filter(x -> x.getVehicleId() == getArguments().getInt(ARGUMENT_CAR_ID)).findAny().orElse(null);
                    car.setName(carName);
                    CarRequests.updateCar(UserConfig.getToken(getContext()), getArguments().getInt(ARGUMENT_CAR_ID), car, new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            Toast.makeText(getContext(), "Информация успешно обновлена", Toast.LENGTH_SHORT);
                            return null;
                        }
                    }, new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
                            dlgAlert.setMessage("Проверьте соединение с интернетом и попробуйте снова");
                            dlgAlert.setTitle("Не удалось обновить информацию об автомобиле");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.create().show();
                            return null;
                        }
                    });
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(itSelf).commit();
                getActivity().getSupportFragmentManager().popBackStack();
                Navigation.findNavController(root).navigate(R.id.nav_automobiles);
            }
        });

        return root;
    }

    private void setCarActive(int carId) {
        Car activeCar = UserData.getInstance().getCar();
        if(activeCar.getVehicleId() != carId) {
            CarRequests.noChooseCar(UserConfig.getToken(getContext()), activeCar.getVehicleId(), new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    CarRequests.chooseCar(UserConfig.getToken(getContext()), carId, new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            Toast.makeText(getContext(), "Автомобиль выбран как основной!", Toast.LENGTH_SHORT);
                            return null;
                        }
                    }, new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
                            dlgAlert.setMessage("Проверьте соединение с интернетом и попробуйте снова");
                            dlgAlert.setTitle("Не удалось выбрать автомобиль как основной");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.create().show();
                            return null;
                        }
                    });
                    return null;
                }
            });
        }
    }

    private void deleteCar() {
        int id = getArguments().getInt(ARGUMENT_CAR_ID);
        boolean isNotActive = id != UserData.getInstance().getCar().getVehicleId();
        List<Car> carList = UserData.getInstance().getCarList();
        if(isNotActive  || carList.isEmpty()) {
            CarRequests.deleteCar(UserConfig.getToken(getContext()), id, new Callable<Void>() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public Void call() throws Exception {
                    Car car = UserData.getInstance().getCarList().stream().filter(x -> x.getVehicleId() == id).findAny().orElse(null);
                    if(car != null)
                        UserData.getInstance().getCarList().remove(car);
                    return null;
                }
            });
        }
    }



}

