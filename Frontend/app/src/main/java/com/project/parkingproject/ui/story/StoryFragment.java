package com.project.parkingproject.ui.story;

import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.ParkingSession;
import com.project.parkingproject.data.services.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StoryFragment extends Fragment {

    private StoryViewModel storyViewModel;
    private Button filterBtn;
    private Button filterFindBtn;
    private ConstraintLayout filterPanel;
    private ScrollView scroll;
    private TextView storyIsEmptyLabel;
    private EditText filterStartTime;
    private EditText filterEndTime;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        storyViewModel =
                new ViewModelProvider(this).get(StoryViewModel.class);
        View root = inflater.inflate(R.layout.fragment_story, container, false);
        List<ParkingSession> sessions = UserData.getInstance().getParkingSessionsList();


        filterPanel = root.findViewById(R.id.story_filterPanel);
        filterBtn = root.findViewById(R.id.story_filterBtn);
        scroll = root.findViewById(R.id.story_scrollView);
        storyIsEmptyLabel = root.findViewById(R.id.story_emptyLabel);
        filterStartTime = root.findViewById(R.id.story_filterStartTime);
        filterEndTime = root.findViewById(R.id.story_filterEndTime);

        filterFindBtn = root.findViewById(R.id.story_filter_FindBtn);

        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterPanel.setVisibility(View.VISIBLE);
                filterPanel.setEnabled(true);
            }
        });


        filterFindBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storyIsEmptyLabel.setVisibility(View.GONE);
                scroll.removeAllViews();
                filterPanel.setVisibility(View.INVISIBLE);
                filterPanel.setEnabled(false);
                if (filterEndTime.getText().toString().isEmpty() || filterStartTime.getText().toString().isEmpty()) {
                    showStory(root, sessions);
                }
                try {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+'hh:ss");
                    Date end = dateFormat.parse(Utils.convertToDateTimeFromDay(filterEndTime.getText().toString()));
                    Date start = dateFormat.parse(Utils.convertToDateTimeFromDay(filterStartTime.getText().toString()));

                    List<ParkingSession> filteredSessions = new ArrayList<>();
                    sessions.forEach(session -> {
                        try {
                            Date sessionStart = dateFormat.parse(session.getTimeStart());
                            Date sessionEnd = dateFormat.parse(session.getTimeFinish());
                            if (sessionStart.compareTo(start) > 0 && sessionEnd.compareTo(end) < 0)
                                filteredSessions.add(session);
                        } catch (ParseException e) {
                        }
                    });
                    showStory(root, filteredSessions);
                } catch (Exception e) {
                    if (!filterEndTime.getText().toString().isEmpty() || !filterStartTime.getText().toString().isEmpty())
                    Toast.makeText(getContext(), "Ошибка в дате! Формат дд.мм.гггг", Toast.LENGTH_SHORT).show();
                }
            }
        });

        showStory(root, sessions);

        return root;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showStory(View root, List<ParkingSession> sessionList) {
        if (sessionList.isEmpty()) {
            storyIsEmptyLabel.setVisibility(View.VISIBLE);
            return;
        }
        scroll.removeAllViews();
        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setGravity(Gravity.LEFT);
        scroll.addView(layout);
        sessionList.forEach(session -> {
            if(session.getCar().getVehicleId().equals(UserData.getInstance().getCar().getVehicleId()) || UserData.getInstance().getCurrentParkingSession() != null
                    && UserData.getInstance().getCurrentParkingSession().getParkingSessionId() != session.getParkingSessionId() && session.getCar().getVehicleId().equals(UserData.getInstance().getCar().getVehicleId())) {
                TextView data = new TextView(getContext());
                data.setText("Название парковки: " + session.getParking().getParkingName() + "\t\tСтоимость: " + session.getPrice() + "р.\n" +
                        "Время: " + session.getStartTimeInSimpleFormat() + " - " + session.getEndTimeInSimpleFormat());
                data.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        100));
                data.setGravity(Gravity.CENTER);
                data.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.text_view_border));

                layout.addView(data);
            }
        });

    }


    @Override
    public void onStop() {
        super.onStop();
        filterPanel.setEnabled(false);
        filterPanel.setVisibility(View.INVISIBLE);
    }
}