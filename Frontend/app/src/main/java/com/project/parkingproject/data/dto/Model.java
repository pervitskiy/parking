package com.project.parkingproject.data.dto;

public class Model {

    private int modelCarId;

    private String modelCarName;

    public int getModelCarId() {
        return modelCarId;
    }

    public void setModelCarId(int modelCarId) {
        this.modelCarId = modelCarId;
    }

    public String getModelCarName() {
        return modelCarName;
    }

    public void setModelCarName(String modelCarName) {
        this.modelCarName = modelCarName;
    }
}
