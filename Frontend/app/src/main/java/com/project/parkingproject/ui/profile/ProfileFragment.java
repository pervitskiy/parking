package com.project.parkingproject.ui.profile;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.User;
import com.project.parkingproject.data.requests.UserRequests;

import java.util.concurrent.Callable;

public class ProfileFragment extends Fragment {

    private ProfileViewModel slideshowViewModel;

    private EditText nameText;
    private EditText surnameText;
    private TextView phoneText;
    private Button acceptButton;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);

        nameText = root.findViewById(R.id.profile_name);
        surnameText = root.findViewById(R.id.profile_surname);
        phoneText = root.findViewById(R.id.profile_phone);
        acceptButton = root.findViewById(R.id.profile_acceptBtn);
        String name = UserData.getInstance().getUser().getFirstName();
        String surname = UserData.getInstance().getUser().getLastName();
        nameText.setText(name != null ? name : "");
        surnameText.setText(surname != null ? surname : "");
        phoneText.setText(UserData.getInstance().getUser().getTelephone());

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = UserData.getInstance().getUser();
                String name = nameText.getText().toString().trim();
                String surname = surnameText.getText().toString().trim();
                String oldUserName = user.getFirstName() != null ? user.getFirstName() : "";
                String oldUserSurname = user.getLastName() != null ? user.getLastName() : "";
                if(!oldUserName.equals(name) && !oldUserSurname.equals(surname)) {
                    user.setFirstName(name);
                    user.setLastName(surname);
                    UserRequests.updateUser(UserConfig.getToken(getContext()), user.getUserId(), user, new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            Toast.makeText(getContext(), "Информация успешно обновлена", Toast.LENGTH_SHORT);
                            return null;
                        }
                    }, new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
                            dlgAlert.setMessage("Проверьте соединение и попробуйте снова");
                            dlgAlert.setTitle("Не удалось обновить информацию пользователя");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.create().show();
                            return null;
                        }
                    });
                }
            }
        });

        return root;
    }
}