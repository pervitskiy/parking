package com.project.parkingproject.data.services;

import com.project.parkingproject.data.dto.Parking;
import com.project.parkingproject.data.dto.ParkingSession;
import com.project.parkingproject.data.dto.ShorParkingInfo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ParkingService {

    @POST("parking/")
    Call<Void> saveParking(@Header("Authorization")String token, @Body Parking[] body);

    @GET("parking/getAll")
    Call<ShorParkingInfo[]> getAllParkings(@Header ("Authorization") String token);

    @GET("parking/{id}/typeVehicle/{typeVehicleId}")
    Call<Parking> getFullParkinInfo(@Header ("Authorization") String token, @Path("id") int parkingId, @Path("typeVehicleId") int typeVehicleId);

    @POST("parking/{id}/subscribe/{vehicleId}")
    Call<ParkingSession> subscribeOnParking(@Header ("Authorization") String token, @Path("id") int parkingId, @Path("vehicleId") int vehicleId, @Body ParkingSession body);

    @PUT("parking/continue/{parkingSessionId}")
    Call<ParkingSession> continueParkingSession(@Header ("Authorization") String token, @Path("parkingSessionId") Long parkingSessionId, @Body ParkingSession body);

    @PUT("parking/{id}")
    Call<Parking> updateParking(@Header("Authorization")String token, @Path("id") int parkingId, @Body Parking body);

    @DELETE("parking/{adminId}/deleteByAdmin/{parkingId}")
    Call<Void> deleteParking(@Header ("Authorization") String token, @Path("adminId") String adminId, @Path("parkingId") int parkingId);

}
