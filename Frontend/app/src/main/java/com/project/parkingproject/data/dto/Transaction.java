package com.project.parkingproject.data.dto;

public class Transaction {

    private double moneyCount;

    private boolean operationType;

    public Transaction(double moneyCount, boolean operationType) {
        this.moneyCount = moneyCount;
        this.operationType = operationType;
    }

    public double getMoneyCount() {
        return moneyCount;
    }

    public boolean isOperationType() {
        return operationType;
    }
}
