package com.project.parkingproject.data.dto;

import java.util.Map;

public class Parking {

    private Integer parkingId;

    private String parkingName;

    private int numberParkingSpace;

    private int numberDisabledSpace;

    private int numberTrackSpace;

    private int price;

    private String startParkingTime;

    private String endParkingTime;

    private Map<Integer, Integer> statistics;

    private MapPoint[] points;


    public Parking(String parkingName, int numberParkingSpace, int numberDisabledParkingSpace, int numberTruckParkingSpace, int price, String timeStart, String timeFinish, MapPoint[] points) {
        this.parkingName = parkingName;
        this.numberParkingSpace = numberParkingSpace;
        this.numberDisabledSpace = numberDisabledParkingSpace;
        this.numberTrackSpace = numberTruckParkingSpace;
        this.price = price;
        this.startParkingTime = timeStart;
        this.endParkingTime = timeFinish;
        this.points = points;
    }

    public Parking(Integer parkingId, String parkingName, int numberParkingSpace, int numberDisabledSpace, int numberTrackSpace, int price, String startParkingTime, String endParkingTime, MapPoint[] points) {
        this.parkingId = parkingId;
        this.parkingName = parkingName;
        this.numberParkingSpace = numberParkingSpace;
        this.numberDisabledSpace = numberDisabledSpace;
        this.numberTrackSpace = numberTrackSpace;
        this.price = price;
        this.startParkingTime = startParkingTime;
        this.endParkingTime = endParkingTime;
        this.points = points;
    }

    public String getParkingName() {
        return parkingName;
    }

    public int getNumberParkingSpace() {
        return numberParkingSpace;
    }

    public int getNumberDisabledSpace() {
        return numberDisabledSpace;
    }

    public int getNumberTrackSpace() {
        return numberTrackSpace;
    }

    public int getPrice() {
        return price;
    }

    public String getStartParkingTime() {
        return startParkingTime;
    }

    public String getEndParkingTime() {
        return endParkingTime;
    }

    public MapPoint[] getPoints() {
        return points;
    }

    public Integer getParkingId() {
        return parkingId;
    }

    public Map<Integer, Integer> getStatistics() {
        return statistics;
    }

    public void setParkingName(String parkingName) {
        this.parkingName = parkingName;
    }

    public void setNumberParkingSpace(int numberParkingSpace) {
        this.numberParkingSpace = numberParkingSpace;
    }

    public void setNumberDisabledSpace(int numberDisabledSpace) {
        this.numberDisabledSpace = numberDisabledSpace;
    }

    public void setNumberTrackSpace(int numberTrackSpace) {
        this.numberTrackSpace = numberTrackSpace;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setStartParkingTime(String startParkingTime) {
        this.startParkingTime = startParkingTime;
    }

    public void setEndParkingTime(String endParkingTime) {
        this.endParkingTime = endParkingTime;
    }
}
