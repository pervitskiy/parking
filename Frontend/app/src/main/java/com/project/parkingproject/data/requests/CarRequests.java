package com.project.parkingproject.data.requests;

import android.util.Log;

import com.project.parkingproject.data.RetrofitSingleton;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.Car;
import com.project.parkingproject.data.dto.CarPostData;
import com.project.parkingproject.data.services.CarService;

import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarRequests {

    public static void createCar(CarPostData carData, String token, Callable<Void> onSuccess, Callable<Void> onFailure) {
        CarService retrofit = RetrofitSingleton.getInstance().create(CarService.class);
        Call<Car> carCall = retrofit.saveCar("Bearer "  + token, carData);
        carCall.enqueue(new Callback<Car>() {
            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                if(response.isSuccessful()) {
                    UserData.getInstance().getCarList().add(response.body());
                    Log.d("REQUEST", "Create car request was succesfull");
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "Create car request wasn't succesfull " + response.code());
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Car> call, Throwable t) {
                Log.e("REQUEST", "Create car request failed");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void getAllUserCars(String token, String userId, Callable<Void> onSucces) {
        CarService retrofit = RetrofitSingleton.getInstance().create(CarService.class);
        Call<Car[]> carCall = retrofit.getUserCars("Bearer "  + token, userId);
        carCall.enqueue(new Callback<Car[]>() {
            @Override
            public void onResponse(Call<Car[]> call, Response<Car[]> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Get cars request was succesfull");
                    UserData.getInstance().initCars(response.body());

                    try {
                        onSucces.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "Get cars request wasn't succesfull " + response.code());

            }

            @Override
            public void onFailure(Call<Car[]> call, Throwable t) {
                Log.e("REQUEST", "Get cars request failed");
                t.printStackTrace();
            }
        });
    }

    public static void chooseCar(String token, Integer carId, Callable<Void> onSuccess, Callable<Void> onFailure) {
        CarService retrofit = RetrofitSingleton.getInstance().create(CarService.class);
        Call<Car> carCall = retrofit.chooseCar("Bearer "  + token, carId);
        carCall.enqueue(new Callback<Car>() {
            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Choose car request was succesfull");
                    UserData.getInstance().setCar(response.body());
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "Choose car request wasn't succesfull " + response.code());
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Car> call, Throwable t) {
                Log.e("REQUEST", "Choose car request failed");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void noChooseCar(String token, Integer carId, Callable<Void> onSuccess) {
        CarService retrofit = RetrofitSingleton.getInstance().create(CarService.class);
        Call<Car> carCall = retrofit.noChooseCar("Bearer "  + token, carId);
        carCall.enqueue(new Callback<Car>() {
            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "UnChoose car request was succesfull");
                    UserData.getInstance().setCar(null);
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "unChoose car request wasn't succesfull " + response.code());

            }

            @Override
            public void onFailure(Call<Car> call, Throwable t) {
                Log.e("REQUEST", "UnChoose car request failed");
                t.printStackTrace();
            }
        });
    }

    public static void updateCar(String token, Integer carId, Car car, Callable<Void> onSuccess, Callable<Void> onFailure) {
        CarService retrofit = RetrofitSingleton.getInstance().create(CarService.class);
        Call<Car> carCall = retrofit.updateCar("Bearer "  + token, carId, car);
        carCall.enqueue(new Callback<Car>() {
            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                if(response.isSuccessful()) {
                    car.setName(response.body().getName());
                    Log.d("REQUEST", "Updatee car request was succesfull");
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "Update car request wasn't succesfull " + response.code());
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Car> call, Throwable t) {
                Log.e("REQUEST", "Update car request failed");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void deleteCar(String token, Integer carId, Callable<Void> onSuccess) {
        CarService retrofit = RetrofitSingleton.getInstance().create(CarService.class);
        Call<Void> carCall = retrofit.deleteCar("Bearer "  + token, carId);
        carCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "delete car request was succesfull");
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "delete car request wasn't succesfull " + response.code());

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("REQUEST", "delete car request failed");
                t.printStackTrace();
            }
        });
    }
}
