package com.project.parkingproject.data.services;

import com.project.parkingproject.data.dto.Car;
import com.project.parkingproject.data.dto.CarPostData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CarService {

    @POST("vehicle/")
    Call<Car> saveCar(@Header("Authorization")String token, @Body CarPostData body);

    @GET("user/{userId}/vehicle/")
    Call<Car[]> getUserCars(@Header ("Authorization") String token, @Path("userId") String id);

    @GET("vehicle/choose_car/{vehicleId}")
    Call<Car> chooseCar(@Header ("Authorization") String token, @Path("vehicleId") Integer vehicleId);

    @GET("vehicle/no_choose_car/{vehicleId}")
    Call<Car> noChooseCar(@Header ("Authorization") String token, @Path("vehicleId") Integer vehicleId);

    @PUT("vehicle/{vehicleId}")
    Call<Car> updateCar(@Header ("Authorization") String token, @Path("vehicleId") Integer vehicleId, @Body Car body);

    @DELETE("vehicle/{vehicleId}")
    Call<Void> deleteCar(@Header ("Authorization") String token, @Path("vehicleId") Integer vehicleId);
}
