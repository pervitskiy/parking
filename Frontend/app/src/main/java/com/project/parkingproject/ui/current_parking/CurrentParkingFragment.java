package com.project.parkingproject.ui.current_parking;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.project.parkingproject.R;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.data.dto.Parking;
import com.project.parkingproject.data.dto.ParkingSession;
import com.project.parkingproject.ui.map.ParkingPaymentFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.project.parkingproject.ui.map.MapFragment.ARGUMENT_BALANCE;
import static com.project.parkingproject.ui.map.MapFragment.ARGUMENT_COST;
import static com.project.parkingproject.ui.map.MapFragment.ARGUMENT_PARKING_ID;
import static com.project.parkingproject.ui.map.MapFragment.ARGUMENT_TIME;


public class CurrentParkingFragment extends Fragment {

    private TextView haveNotCurrentPkgText;
    private TextView currentParkingName;
    private TextView currentParkingStartTime;
    private TextView currentParkingEndTime;
    private TextView currentParkingPrice;
    private Button extendBtn;


    //labels
    private TextView currentParkingNameLabel;
    private TextView currentParkingStartTimeLabel;
    private TextView currentParkingEndTimeLabel;
    private TextView currentParkingPriceLabel;

    private final CurrentParkingFragment itSelf = this;
    private ParkingPaymentFragment fragment = new ParkingPaymentFragment();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_current_parking, container, false);

        haveNotCurrentPkgText = root.findViewById(R.id.current_parking_not_current_parking_text);

        currentParkingName = root.findViewById(R.id.current_parking_name);
        currentParkingStartTime = root.findViewById(R.id.current_parking_startTime);
        currentParkingEndTime = root.findViewById(R.id.current_parking_endTime);
        currentParkingPrice = root.findViewById(R.id.current_parking_price);
        extendBtn = root.findViewById(R.id.current_parking_extendBtn);

        currentParkingNameLabel = root.findViewById(R.id.current_parking_name_label);
        currentParkingStartTimeLabel = root.findViewById(R.id.current_parking_startTime_label);
        currentParkingEndTimeLabel = root.findViewById(R.id.current_parking_endTime_label);
        currentParkingPriceLabel = root.findViewById(R.id.current_parking_price_label);

        ParkingSession session = UserData.getInstance().getCurrentParkingSession();

        if (session == null) {
            haveNotCurrentPkgText.setVisibility(View.VISIBLE);
        } else {
            showParkingSessionData(session, root);
        }

        return root;
    }

    private void showParkingSessionData(ParkingSession session, View root) {
        currentParkingName.setVisibility(View.VISIBLE);
        currentParkingStartTime.setVisibility(View.VISIBLE);
        currentParkingEndTime.setVisibility(View.VISIBLE);
        currentParkingPrice.setVisibility(View.VISIBLE);

        currentParkingNameLabel.setVisibility(View.VISIBLE);
        currentParkingStartTimeLabel.setVisibility(View.VISIBLE);
        currentParkingEndTimeLabel.setVisibility(View.VISIBLE);
        currentParkingPriceLabel.setVisibility(View.VISIBLE);

        Parking parking = session.getParking();

        currentParkingName.setText(parking.getParkingName());
        currentParkingStartTime.setText(session.getStartTimeInSimpleFormat());
        currentParkingEndTime.setText(session.getEndTimeInSimpleFormat());
        currentParkingPrice.setText(Integer.toString(session.getPrice()));
        extendBtn.setVisibility(View.VISIBLE);
        extendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int time = 0;
                try {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+'hh:ss");
                    Date end = dateFormat.parse(parking.getEndParkingTime());
                    Date currentEnd = dateFormat.parse(session.getTimeFinish());
                    int endHour = end.getHours();
                    int endMinute = end.getMinutes();
                    int currentEndHour = currentEnd.getHours() + 3;
                    int currentEndMinute = currentEnd.getMinutes();
                    //time = end.getHours() - currentEnd.getHours() + 1;
                    if (endHour <= currentEndHour) {
                        Toast.makeText(getContext(), "Парковку нельзя продлить дольше", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    if (endMinute > currentEndMinute) {
                        time = endHour - currentEndHour;
                    } else {
                        time = endHour - currentEndHour - 1;
                    }

                } catch (ParseException e) {
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putInt(ARGUMENT_COST, parking.getPrice());
                bundle.putDouble(ARGUMENT_BALANCE, UserData.getInstance().getUser().getMoney());
                bundle.putInt(ARGUMENT_PARKING_ID, parking.getParkingId());
                bundle.putInt(ARGUMENT_TIME, time);
                fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, fragment).addToBackStack(null).commit();
                //getActivity().getSupportFragmentManager().beginTransaction().remove(itSelf).addToBackStack(null).commit();
                root.setVisibility(View.GONE);
                root.setEnabled(false);
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        } catch (Exception e) {
        }
        try {
            getActivity().getSupportFragmentManager().beginTransaction().remove(itSelf).commit();
        } catch (Exception e) {
        }
        getActivity().getSupportFragmentManager().getFragments().clear();
    }


}