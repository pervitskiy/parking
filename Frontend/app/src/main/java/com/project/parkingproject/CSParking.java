package com.project.parkingproject;

import android.app.Application;

import com.yandex.mapkit.MapKitFactory;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

public class CSParking extends Application {

    private final String APP_METRICA_API_KEY = "d397a842-37ba-4418-a840-3ed825e64eb5";
    private final String MAPKIT_API_KEY = "e7ad3f9e-56af-439e-9944-55910efd1b2d";

    @Override
    public void onCreate() {
        super.onCreate();
        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder(APP_METRICA_API_KEY).build();
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(getApplicationContext(), config);
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this);
        MapKitFactory.setApiKey(MAPKIT_API_KEY);
    }
}
