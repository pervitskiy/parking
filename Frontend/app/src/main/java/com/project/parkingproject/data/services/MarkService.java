package com.project.parkingproject.data.services;

import com.project.parkingproject.data.dto.Mark;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface MarkService{
    @GET("mark/getAll")
    Call<Mark[]> getAllMarks(@Header("Authorization") String token);
}
