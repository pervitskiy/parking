package com.project.parkingproject.data.requests;

import android.util.Log;

import com.project.parkingproject.data.RetrofitSingleton;
import com.project.parkingproject.data.dto.User;
import com.project.parkingproject.data.services.UserService;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllUsersRequest {

    private List<User> data;

    public void getAllUsers (String token, String adminId, Callable<Void> onSuccess) {
        UserService retrofit = RetrofitSingleton.getInstance().create(UserService.class);
        Call<User[]> getAllCall = retrofit.getAllUsers("Bearer " + token, adminId);
        getAllCall.enqueue(new Callback<User[]>() {
            @Override
            public void onResponse(Call<User[]> call, Response<User[]> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "GET All USERs REQUEST SUCCESFULL");
                    data = Arrays.asList(response.body());

                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "GET all USERs request wasn't succesfull " + response.code());
            }

            @Override
            public void onFailure(Call<User[]> call, Throwable t) {
                Log.e("REQUEST", "GET all USERs request wasn't succesfull");
                t.printStackTrace();
            }
        });
    }

    public List<User> getData() {
        return data;
    }
}
