package com.project.parkingproject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.project.parkingproject.data.UserConfig;
import com.project.parkingproject.data.UserData;
import com.project.parkingproject.ui.login.LoginActivity;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;
import com.yandex.mapkit.MapKitFactory;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.customview.widget.Openable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.List;

import static androidx.navigation.ui.NavigationUI.onNavDestinationSelected;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MapKitFactory.initialize(this);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (UserConfig.isAdmin(getApplicationContext())) {
            navigationView.getMenu().setGroupVisible(R.id.admin_group, true);
            navigationView.getMenu().setGroupEnabled(R.id.admin_group, true);
            mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_users,
                    R.id.nav_map, R.id.nav_automobiles, R.id.nav_profile, R.id.nav_wallet,
                    R.id.nav_current_parking, R.id.nav_story, R.id.nav_exit)
                    .setDrawerLayout(drawer)
                    .build();
        } else {
            mAppBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.nav_map, R.id.nav_automobiles, R.id.nav_profile, R.id.nav_wallet,
                    R.id.nav_current_parking, R.id.nav_story, R.id.nav_exit)
                    .setDrawerLayout(drawer)
                    .build();
        }

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //clearFragments();
                if (item.getItemId() == R.id.nav_exit) {
                    android.app.AlertDialog.Builder dlgAlert = new android.app.AlertDialog.Builder(MainActivity.this);
                    dlgAlert.setMessage("Вы действительно хотите выйти?");
                    dlgAlert.setTitle("Выход");
                    AlertDialog dialog;
                    dlgAlert.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            UserConfig.clearUserInfo(getApplicationContext());
                            UserData.getInstance().clearAllInfo();
                            startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        }
                    });
                    dlgAlert.setCancelable(true);
                    dlgAlert.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog = dlgAlert.create();
                    dialog.show();
                    return true;
                } else {
                    boolean handled = onNavDestinationSelected(item, navController);
                    if (handled) {
                        ViewParent parent = navigationView.getParent();
                        if (parent instanceof Openable) {
                            ((Openable) parent).close();
                        } else {
                            BottomSheetBehavior bottomSheetBehavior =
                                    findBottomSheetBehavior(navigationView);
                            if (bottomSheetBehavior != null) {
                                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                            }
                        }
                    }

                    return handled;
                }
            }
        });
//        findViewById(R.id.main_fragment_container).setVisibility(View.INVISIBLE);
//        findViewById(R.id.main_fragment_container).setEnabled(false);
    }


    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    static BottomSheetBehavior findBottomSheetBehavior(@NonNull View view) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        if (!(params instanceof CoordinatorLayout.LayoutParams)) {
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                return findBottomSheetBehavior((View) parent);
            }
            return null;
        }
        CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams) params)
                .getBehavior();
        if (!(behavior instanceof BottomSheetBehavior)) {
            // We hit a CoordinatorLayout, but the View doesn't have the BottomSheetBehavior
            return null;
        }
        return (BottomSheetBehavior) behavior;
    }

    @Override
    protected void onStop() {
        super.onStop();

        MapKitFactory.getInstance().onStop();
    }

    public AppBarConfiguration getmAppBarConfiguration() {
        return mAppBarConfiguration;
    }

    private void clearFragments() {
        List<Fragment> al = getSupportFragmentManager().getFragments();
        if (al == null) {
            return;
        }

        for (Fragment frag : al) {
            if (frag == null) {
                continue;
            }
            getSupportFragmentManager().beginTransaction().remove(frag).commit();
        }
    }

}