package com.project.parkingproject.data.dto;

import com.yandex.mapkit.geometry.Point;

public class MapPoint {

    private String x;

    private String y;

    public MapPoint(String x, String y) {
        this.x = x;
        this.y = y;
    }

    public MapPoint(Point mapPoint) {
        this.x = String.valueOf(mapPoint.getLatitude());
        this.y = String.valueOf(mapPoint.getLongitude());
    }

    public String getX() {
        return x;
    }

    public String getY() {
        return y;
    }
}
