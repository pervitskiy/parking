package com.project.parkingproject.data;

import android.content.Context;
import android.content.SharedPreferences;

public class UserConfig {

    private static final String USER_INFO = "User_Info";
    private static final String TOKEN = "Token";
    private static final String USER_ID = "User_Id";
    private static final String ADMIN = "Admin";

    public static void saveUserInfo(String id, String token, boolean isAdmin, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOKEN, token);
        editor.putString(USER_ID, id);
        editor.putBoolean(ADMIN, isAdmin);
        editor.apply();
    }

    public static String getToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        return sharedPreferences.getString(TOKEN, "");
    }

    public static String getUserId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_ID, "");
    }

    public static boolean isAdmin(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(ADMIN,false);
    }

    public static void clearUserInfo(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(TOKEN);
        editor.remove(USER_ID);
        editor.apply();
    }
}
