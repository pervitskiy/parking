package com.project.parkingproject.data.requests;

import android.util.Log;

import com.project.parkingproject.data.RetrofitSingleton;
import com.project.parkingproject.data.dto.Mark;
import com.project.parkingproject.data.services.MarkService;

import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarkRequests {

    private Mark[] result;

    public void getAllMarks(String token, Callable<Void> onSuccess, Callable<Void> onFailure) {
        MarkService retrofit = RetrofitSingleton.getInstance().create(MarkService.class);
        Call<Mark[]> parkingsCall = retrofit.getAllMarks("Bearer "  + token);
        parkingsCall.enqueue(new Callback<Mark[]>() {
            @Override
            public void onResponse(Call<Mark[]> call, Response<Mark[]> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Get all marks request was succesfull");
                    result = response.body();

                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "Get mark request wasn't succesfull" + response.code());
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Mark[]> call, Throwable t) {
                Log.e("REQUEST", "Getting marks request failed");
                t.printStackTrace();
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Mark[] getResult() {
        return result;
    }
}
