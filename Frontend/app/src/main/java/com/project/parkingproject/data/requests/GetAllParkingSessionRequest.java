package com.project.parkingproject.data.requests;

import android.util.Log;

import com.project.parkingproject.data.RetrofitSingleton;
import com.project.parkingproject.data.dto.ParkingSession;
import com.project.parkingproject.data.services.UserService;

import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetAllParkingSessionRequest {

    private ParkingSession[] result;

    public void getAllParkingSessionsRequest(String token, String userId, Callable<Void> onSucess) {
        UserService retrofit = RetrofitSingleton.getInstance().create(UserService.class);
        Call<ParkingSession[]> sessionCall = retrofit.getAllParkingSession("Bearer "  + token, userId);
        sessionCall.enqueue(new Callback<ParkingSession[]>() {
            @Override
            public void onResponse(Call<ParkingSession[]> call, Response<ParkingSession[]> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "Get all parking session request was succesfull");
                    result = response.body();

                    try {
                        onSucess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }

                Log.e("REQUEST", "Get all parking session wasn't succesfull" + response.code());
            }

            @Override
            public void onFailure(Call<ParkingSession[]> call, Throwable t) {
                Log.e("REQUEST", "Getting full parking request failed");
                t.printStackTrace();
            }
        });
    }

    public ParkingSession[] getResult() {
        return result;
    }
}
