package com.project.parkingproject.data;

import com.project.parkingproject.data.dto.Car;
import com.project.parkingproject.data.dto.ParkingSession;
import com.project.parkingproject.data.dto.ShorParkingInfo;
import com.project.parkingproject.data.dto.User;
import com.project.parkingproject.data.requests.CarRequests;
import com.project.parkingproject.data.requests.GetAllParkingSessionRequest;
import com.project.parkingproject.data.requests.ParkingRequests;
import com.project.parkingproject.data.requests.UserRequests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

public class UserData {

    private static volatile UserData userData;

    private List<ShorParkingInfo> parkingList;

    private List<Car> carList;

    private List<ParkingSession> parkingSessionsList;

    private User user;

    private Car car;

    private ParkingSession currentParkingSession;

    public static UserData getInstance() {
        if(userData == null)
            synchronized (UserData.class) {
                if(userData == null) {
                    userData = new UserData();
                }
            }
        return userData;
    }

    public void initParkings(ShorParkingInfo[] parkings) {
        parkingList = Arrays.asList(parkings);
    }


    public void initCars(Car[] cars) {
        carList = new ArrayList<>(Arrays.asList(cars));
    }

    public void initParkingSessions(ParkingSession[] parkingSessions) {
        parkingSessionsList = new ArrayList<>(Arrays.asList(parkingSessions));
    }

    public void initUser(User user) {
        this.user = user;
    }

    public void requestUserData(String token, String userId) {
        ParkingRequests.getAllParkings(token, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                return null;
            }
        });

        CarRequests.getAllUserCars(token, userId, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                return null;
            }
        });

        UserRequests.getUser(token, userId);

        GetAllParkingSessionRequest getAllParkingSessionRequest = new GetAllParkingSessionRequest();
        getAllParkingSessionRequest.getAllParkingSessionsRequest(token, userId, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                initParkingSessions(getAllParkingSessionRequest.getResult());
                return null;
            }
        });
    }

    public List<ShorParkingInfo> getParkingList() {
        return parkingList;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public User getUser() {
        return user;
    }

    public Car getCar() {
        if(car == null && carList != null) {
            for (Car c : carList) {
                if(c.isChosen()) {
                    car = c;
                    break;
                }
            }
        }
        return car;
    }

    public ParkingSession getCurrentParkingSession(){
        if(currentParkingSession == null) {
            currentParkingSession = findCurrent();
        }else{
            try {
                Date finish = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+'hh:ss").parse(currentParkingSession.getTimeFinish());
                if (!currentParkingSession.getCar().getVehicleId().equals(car.getVehicleId()) || finish.compareTo(new Date()) > 0) {
                    currentParkingSession = findCurrent();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return currentParkingSession;
    }

    private ParkingSession findCurrent() {
        if(parkingSessionsList != null && !parkingSessionsList.isEmpty()) {
            for (ParkingSession session : parkingSessionsList) {
                if (session.getCar().getVehicleId().equals(car.getVehicleId())) {
                    try {
                        Date finish = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+'00:00").parse(session.getTimeFinish());
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(new Date());
                        calendar.add(Calendar.HOUR_OF_DAY, -3);
                        Date time = calendar.getTime();
                        if (finish.compareTo(time) > 0) {
                            return session;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
            return null;
    }

    public List<ParkingSession> getParkingSessionsList() {
        return parkingSessionsList;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setCurrentParkingSession(ParkingSession currentParkingSession) {
        this.currentParkingSession = currentParkingSession;
    }

    public void clearAllInfo() {
        parkingList = null;
        carList = null;
        user = null;
        parkingSessionsList = null;
        currentParkingSession = null;
        car = null;
    }
}
